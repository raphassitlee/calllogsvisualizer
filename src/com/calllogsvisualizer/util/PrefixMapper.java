package com.calllogsvisualizer.util;

import com.calllogsvisualizer.orm.Carrier;
import com.calllogsvisualizer.orm.Prefix;
import com.calllogsvisualizer.orm.UserDefinedNumber;

public class PrefixMapper {

	public static boolean isInternational(String number) {
		if (number.length() == 12 && number.startsWith("+61")) {
			return false;
		}
		else if (number.startsWith("+") || number.startsWith("0011")
				|| number.startsWith("0014") || number.startsWith("0015")
				|| number.startsWith("0016") || number.startsWith("0019")) {
			return true;
		}
		return false;
	}

	public static Carrier getCarrierByNumber(String number) {
		// Check with User defined first
		String toFindNumber = number;
		if(toFindNumber.length() == 12 && toFindNumber.startsWith("+61")) {
			toFindNumber = "0" + toFindNumber.substring(3);
		}
		UserDefinedNumber userDefined = UserDefinedNumber.getByNumber(toFindNumber);
		if(userDefined != null) {
			return Carrier.findById(userDefined.getCarrierId());
		}
		
		// Look up prefix table
		String numberPrefix;
		if (number.startsWith("+")) {
			numberPrefix = number.substring(3, 7);
		} else {
			numberPrefix = number.substring(1, 5);
		}
		Prefix prefix = Prefix.findByNumberPrefix(numberPrefix);
		return Carrier.findById(prefix.getCarrierId());
	}

	public static boolean isMobileNumber(String number) {
		if (number.length() == 10 && number.startsWith("04"))
			return true;
		else if (number.length() == 12 && number.startsWith("+614"))
			return true;
		else
			return false;
	}
}
