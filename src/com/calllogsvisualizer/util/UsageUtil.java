package com.calllogsvisualizer.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import android.content.Context;

import com.calllogsvisualizer.orm.Carrier;
import com.calllogsvisualizer.orm.DailyCarrierSummary;
import com.calllogsvisualizer.orm.DailyRegionSummary;
import com.calllogsvisualizer.ui.LogsListData;

public class UsageUtil {

	private static final int YESTERDAY = -1;
	private static final int TODAY = 0;
	private static final int TOMORROW = 1;
	
	private static Context context;
	
	public static void reCreateDailyCarrierSummary() {
		DailyCarrierSummary.deleteAll();
		updateDailySummary(context);
	}

	public static void updateDailySummary(Context context) {
		UsageUtil.context = context;
		
		ArrayList<LogsListData> logsList = new ArrayList<LogsListData>();
		
		DailyCarrierSummary latestDailyCarrier = DailyCarrierSummary
				.getLatestByDate();
		if (latestDailyCarrier != null) {
			long latestCarrierDate = latestDailyCarrier.getDate();
			if (latestCarrierDate < getDateExcludeTime(YESTERDAY).getTime()) {
				// update
				logsList = CallLogsUtil.getCallDetails(context,
						getTomorrowDate(new Date(latestCarrierDate)), getDateExcludeTime(YESTERDAY));
			}
		} else {
			logsList = CallLogsUtil.getCallDetailsFromBeginning(context,
					getDateExcludeTime(YESTERDAY));
		}
		
		if(!logsList.isEmpty()){
			for (int i = 0; i < logsList.size(); i++) {
				if(logsList.get(i).isMobileNumber()){
					long date = getDateExcludeTime(logsList.get(i).getDatetime()).getTime();
					Carrier operator = logsList.get(i).getOperator();
					long duration = Long.parseLong(logsList.get(i).getDuration());

					DailyCarrierSummary oldRecord = DailyCarrierSummary.getByDateAndCarrierId(date, operator.getId());
					if(oldRecord != null) {
						oldRecord.setDuration(oldRecord.getDuration() + duration);
						oldRecord.save();
					} else {
						DailyCarrierSummary newRecord = new DailyCarrierSummary(date, operator.getId(), duration);
						newRecord.save();
					}
				}
			}
		}

		logsList = new ArrayList<LogsListData>();
		
		DailyRegionSummary latestDailyRegion = DailyRegionSummary
				.getLatestByDate();
		if (latestDailyRegion != null) {
			long latestRegionDate = latestDailyRegion.getDate();
			if (latestRegionDate < getDateExcludeTime(YESTERDAY).getTime()) {
				logsList = CallLogsUtil.getCallDetails(context,
						getTomorrowDate(new Date(latestRegionDate)), getDateExcludeTime(YESTERDAY));
			}
		} else {
			logsList = CallLogsUtil.getCallDetailsFromBeginning(context,
					getDateExcludeTime(YESTERDAY));
		}
		
		if(!logsList.isEmpty()){
			for (int i = 0; i < logsList.size(); i++) {
				long date = getDateExcludeTime(logsList.get(i).getDatetime()).getTime();
				boolean international = logsList.get(i).isInternational();
				long duration = Long.parseLong(logsList.get(i).getDuration());

				DailyRegionSummary oldRecord = DailyRegionSummary.getByDateAndInternational(date, international);
				if (oldRecord != null) {
					oldRecord.setDuration(oldRecord.getDuration() + duration);
					oldRecord.save();
				} else {
					DailyRegionSummary newRecord = new DailyRegionSummary(date,international,duration);
					newRecord.save();
				}
			}
		}
	}

	public static Date getTomorrowDate(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_MONTH, 1);
		String strDate = calendar.get(Calendar.DAY_OF_MONTH) + "-"
				+ (calendar.get(Calendar.MONTH)+1) + "-"
				+ calendar.get(Calendar.YEAR);
		DateFormat formatter = new SimpleDateFormat("d-MM-yyyy");
		Date tomorrowDate = null;
		try {
			tomorrowDate = formatter.parse(strDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tomorrowDate;
	}
	
	public static Date getDateExcludeTime(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		String strDate = calendar.get(Calendar.DAY_OF_MONTH) + "-"
				+ (calendar.get(Calendar.MONTH)+1) + "-"
				+ calendar.get(Calendar.YEAR);
		DateFormat formatter = new SimpleDateFormat("d-MM-yyyy");
		Date dateExlucdeTime = null;
		try {
			dateExlucdeTime = formatter.parse(strDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dateExlucdeTime;
	}
	
	public static Date getDateExcludeTime(long date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(date);
		String strDate = calendar.get(Calendar.DAY_OF_MONTH) + "-"
				+ (calendar.get(Calendar.MONTH)+1) + "-"
				+ calendar.get(Calendar.YEAR);
		DateFormat formatter = new SimpleDateFormat("d-MM-yyyy");
		Date dateExlucdeTime = null;
		try {
			dateExlucdeTime = formatter.parse(strDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dateExlucdeTime;
	}

	public static Date getDateExcludeTime(int day) {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_YEAR, day);
		String strDate = calendar.get(Calendar.DAY_OF_MONTH) + "-"
				+ (calendar.get(Calendar.MONTH)+1) + "-"
				+ calendar.get(Calendar.YEAR);
		DateFormat formatter = new SimpleDateFormat("d-MM-yyyy");
		Date date = null;
		try {
			date = formatter.parse(strDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}
	
	public static int getTodayDayOfMonth() {
		Calendar calendar = Calendar.getInstance();
		return calendar.get(Calendar.DAY_OF_MONTH);
	}
	
	public static List<Long[]> getCycleDates(int cycleStartDayOfMonth) {
		List<Long[]> datePairs = new ArrayList<Long[]>();
		Long[] pair = new Long[2];
		
		DailyCarrierSummary oldest = DailyCarrierSummary.getOldestByDate();
		Calendar calendarOldest = Calendar.getInstance();
		if(oldest != null)
			calendarOldest.setTimeInMillis(oldest.getDate());
		else
			calendarOldest.setTimeInMillis(CallLogsUtil.getOldestCallDetail().getDatetime().getTime());
		
		Calendar calendar = Calendar.getInstance();
		
		calendar.set(Calendar.DAY_OF_MONTH, cycleStartDayOfMonth);
		if(calendar.get(Calendar.DAY_OF_MONTH) < cycleStartDayOfMonth) {
			calendar.add(Calendar.DAY_OF_MONTH, -1);
		}
		
		while (calendarOldest.getTimeInMillis() < calendar.getTimeInMillis()) {
			pair = new Long[2];
			calendar.add(Calendar.DAY_OF_MONTH, -1);
			pair[1] = calendar.getTimeInMillis();
			
			calendar.add(Calendar.MONTH, -1);
			calendar.add(Calendar.DAY_OF_MONTH, 1);
			
			pair[0] = calendar.getTimeInMillis();
			datePairs.add(pair);
		}
		
		return datePairs;
	}
	
	public static List<Long[]> getMonthDates() {
		List<Long[]> datePairs = new ArrayList<Long[]>();
		Long[] pair = new Long[2];
		
		DailyCarrierSummary oldestCarrier = DailyCarrierSummary.getOldestByDate();
		DailyRegionSummary oldestRegion = DailyRegionSummary.getOldestByDate();
		Calendar calendarOldest = Calendar.getInstance();
		if(oldestCarrier != null && oldestRegion == null) {
			calendarOldest.setTimeInMillis(oldestCarrier.getDate());
		} else if (oldestCarrier == null && oldestRegion != null) {
			calendarOldest.setTimeInMillis(oldestRegion.getDate());
		} else if (oldestCarrier != null && oldestRegion != null) {
			if(oldestCarrier.getDate() > oldestRegion.getDate()) {
				calendarOldest.setTimeInMillis(oldestRegion.getDate());
			} else {
				calendarOldest.setTimeInMillis(oldestCarrier.getDate());
			}
		} else {
			calendarOldest.setTimeInMillis(CallLogsUtil.getOldestCallDetail().getDatetime().getTime());
		}
		
		Calendar calendar = Calendar.getInstance();
		DateFormat formatter = new SimpleDateFormat("d-MM-yyyy");
		Date date = null;
		String strDate;
		
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		
		while (calendarOldest.getTimeInMillis() < calendar.getTimeInMillis()) {
			pair = new Long[2];
			calendar.add(Calendar.DAY_OF_MONTH, -1);
			
			strDate = calendar.get(Calendar.DAY_OF_MONTH) + "-" + (calendar.get(Calendar.MONTH)+1) + "-" + calendar.get(Calendar.YEAR);
			try {
				date = formatter.parse(strDate);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			pair[1] = date.getTime();
			
			while (true) {
				calendar.add(Calendar.DAY_OF_YEAR, -1);
				if(calendar.get(Calendar.DAY_OF_MONTH) == 1)
					break;
		    }
			strDate = calendar.get(Calendar.DAY_OF_MONTH) + "-" + (calendar.get(Calendar.MONTH)+1) + "-" + calendar.get(Calendar.YEAR);
			try {
				date = formatter.parse(strDate);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			pair[0] = date.getTime();
			datePairs.add(pair);
		}
		
		return datePairs;
	}
	
	public static Long[] getThisMonthDates() {
		Long[] pair = new Long[2];
		
		Calendar calendar = Calendar.getInstance();
		DateFormat formatter = new SimpleDateFormat("d-MM-yyyy");
		Date date = null;
		String strDate;
		
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		
			pair = new Long[2];
			
			strDate = calendar.get(Calendar.DAY_OF_MONTH) + "-" + (calendar.get(Calendar.MONTH)+1) + "-" + calendar.get(Calendar.YEAR);
			try {
				date = formatter.parse(strDate);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			pair[0] = date.getTime();
			
			while (true) {
				calendar.add(Calendar.DAY_OF_YEAR, 1);
				if(calendar.get(Calendar.DAY_OF_MONTH) == 1) {
					calendar.add(Calendar.DAY_OF_YEAR, -1);
					break;
				}
		    }
			strDate = calendar.get(Calendar.DAY_OF_MONTH) + "-" + (calendar.get(Calendar.MONTH)+1) + "-" + calendar.get(Calendar.YEAR);
			try {
				date = formatter.parse(strDate);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			pair[1] = date.getTime();
		
		return pair;
	}

}
