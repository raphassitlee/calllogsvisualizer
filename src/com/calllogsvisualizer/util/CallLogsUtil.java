package com.calllogsvisualizer.util;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import com.calllogsvisualizer.orm.Prefix;
import com.calllogsvisualizer.ui.LogsListData;
import com.calllogsvisualizer.ui.TodayDurations;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.PhoneLookup;
import android.widget.ImageView;

public class CallLogsUtil {

	private static Context context;

	private static ArrayList<LogsListData> logsListData;

	public static ArrayList<LogsListData> getCallDetails(Context context,
			Date startDate, Date endDate) {
		CallLogsUtil.context = context;
		retrieveCallDetails();

		ArrayList<LogsListData> logsListDataByDate = new ArrayList<LogsListData>();

		for (int i = 0; i < logsListData.size(); i++) {
			if ((UsageUtil.getDateExcludeTime(logsListData.get(i).getDatetime()).after(startDate) || UsageUtil.getDateExcludeTime(logsListData.get(i).getDatetime()).equals(startDate)) && UsageUtil.getDateExcludeTime(logsListData.get(i).getDatetime()).before(endDate)) {
				logsListDataByDate.add(logsListData.get(i));
			} else if (UsageUtil.getDateExcludeTime(logsListData.get(i).getDatetime()).equals(endDate)) {
				logsListDataByDate.add(logsListData.get(i));
			} else if (UsageUtil.getDateExcludeTime(logsListData.get(i).getDatetime()).after(endDate)) {
				break;
			}
		}

		return logsListDataByDate;
	}
	
	public static ArrayList<LogsListData> getCallDetailsFromBeginning(Context context,
			Date date) {
		CallLogsUtil.context = context;
		retrieveCallDetails();

		ArrayList<LogsListData> logsListDataByDate = new ArrayList<LogsListData>();

		for (int i = 0; i < logsListData.size(); i++) {
			if (UsageUtil.getDateExcludeTime(logsListData.get(i).getDatetime()).before(date) || UsageUtil.getDateExcludeTime(logsListData.get(i).getDatetime()).equals(date)) {
				logsListDataByDate.add(logsListData.get(i));
			}
		}

		return logsListDataByDate;
	}
	
	public static ArrayList<LogsListData> getTodayCallDetails(Context context) {
		CallLogsUtil.context = context;
		retrieveCallDetails();

		ArrayList<LogsListData> logsListDataByDate = new ArrayList<LogsListData>();

		for (int i = 0; i < logsListData.size(); i++) {
			if (UsageUtil.getDateExcludeTime(logsListData.get(i).getDatetime()).equals(UsageUtil.getDateExcludeTime(new Date()))) {
				logsListDataByDate.add(logsListData.get(i));
			}
		}

		return logsListDataByDate;
	}
	
	public static TodayDurations getTodayCallDetailDurations(Context context) {
		CallLogsUtil.context = context;
		retrieveCallDetails();

		long todayDurationDomestic = 0;
		long todayDurationInternational = 0;
		long todayDurationOptus = 0;
		long todayDurationVodafone = 0;
		long todayDurationTelstra = 0;
		long todayDurationLycamobile = 0;
		
		for (int i = 0; i < logsListData.size(); i++) {
			if (UsageUtil.getDateExcludeTime(logsListData.get(i).getDatetime()).equals(UsageUtil.getDateExcludeTime(new Date()))) {
				if(logsListData.get(i).isInternational()) {
					todayDurationInternational += Long.parseLong(logsListData.get(i).getDuration());
				} else {
					todayDurationDomestic += Long.parseLong(logsListData.get(i).getDuration());
					if(logsListData.get(i).isMobileNumber()) {
						if (logsListData.get(i).getOperator().getId() == 1)
							todayDurationTelstra += Long.parseLong(logsListData.get(i).getDuration());
						else if (logsListData.get(i).getOperator().getId() == 2)
							todayDurationOptus += Long.parseLong(logsListData.get(i).getDuration());
						else if (logsListData.get(i).getOperator().getId() == 3)
							todayDurationVodafone += Long.parseLong(logsListData.get(i).getDuration());
						else if (logsListData.get(i).getOperator().getId() == 4)
							todayDurationLycamobile += Long.parseLong(logsListData.get(i).getDuration());
					}
				}
			}
		}
		
		TodayDurations durations = new TodayDurations();
		durations.setTodayDurationDomestic(todayDurationDomestic);
		durations.setTodayDurationInternational(todayDurationInternational);
		durations.setTodayDurationLycamobile(todayDurationLycamobile);
		durations.setTodayDurationOptus(todayDurationOptus);
		durations.setTodayDurationTelstra(todayDurationTelstra);
		durations.setTodayDurationVodafone(todayDurationVodafone);

		return durations;
	}

	public static ArrayList<LogsListData> getCallDetails(Context context) {
		CallLogsUtil.context = context;
		retrieveCallDetails();
		return logsListData;
	}
	
	public static LogsListData getOldestCallDetail() {
		return logsListData.get(logsListData.size() -1);
	}
	
	public static void init(Context context) {
		CallLogsUtil.context = context;
		retrieveCallDetails();
	}
	
	public static void reInit() {
		logsListData = null;
		retrieveCallDetails();
	}

	private static void retrieveCallDetails() {
		if (logsListData == null) {
			String columns[] = new String[] { CallLog.Calls._ID,
					CallLog.Calls.NUMBER, CallLog.Calls.DATE,
					CallLog.Calls.DURATION, CallLog.Calls.TYPE };

			// select only outgoing calls
			String selection = android.provider.CallLog.Calls.TYPE + " = "
					+ android.provider.CallLog.Calls.OUTGOING_TYPE;

			// order by date
			String strOrder = android.provider.CallLog.Calls.DATE + " DESC";

			// can use either of these two (same)
			// Uri callUri = Uri.parse("content://call_log/calls");
			Uri callUri = CallLog.Calls.CONTENT_URI;

			Cursor c = context.getContentResolver().query(callUri, columns,
					selection, null, strOrder);

			int rowId = c.getColumnIndex(CallLog.Calls._ID);
			int number = c.getColumnIndex(CallLog.Calls.NUMBER);
			int date = c.getColumnIndex(CallLog.Calls.DATE);
			int duration = c.getColumnIndex(CallLog.Calls.DURATION);
			int type = c.getColumnIndex(CallLog.Calls.TYPE);

			ArrayList<LogsListData> logsList = new ArrayList<LogsListData>();

			while (c.moveToNext()) {
				long id = Long.parseLong(c.getString(rowId));
				String phNumber = c.getString(number);
				String callType = c.getString(type);
				String callDate = c.getString(date);
				Date callDayTime = new Date(Long.valueOf(callDate));
				String callDuration = c.getString(duration);

				// for determining call type
				// String dir = null;
				// int dircode = Integer.parseInt(callType);
				// switch (dircode) {
				// case CallLog.Calls.OUTGOING_TYPE:
				// dir = "OUTGOING";
				// break;
				//
				// case CallLog.Calls.INCOMING_TYPE:
				// dir = "INCOMING";
				// break;
				//
				// case CallLog.Calls.MISSED_TYPE:
				// dir = "MISSED";
				// break;
				// }

				LogsListData logData = new LogsListData();

				String[] contact = getContactIdAndName(phNumber);
				String contactName = contact[1];
				if (contactName != null)
					logData.setContactName(contactName);
				else
					logData.setContactName(phNumber);
				if (contact[0] != null)
					logData.setContactImgUri(getContactPhotoUri(Long
							.parseLong(contact[0])));
				else
					logData.setContactImgUri(null);

				logData.setDatetime(callDayTime);
				logData.setDuration(callDuration);
				logData.setId(id);
				logData.setNumber(phNumber);

				if (PrefixMapper.isInternational(phNumber)) {
					logData.setInternational(true);
				} else {
					logData.setInternational(false);
					if (PrefixMapper.isMobileNumber(phNumber)) {
						logData.setMobileNumber(true);
						logData.setOperator(PrefixMapper
								.getCarrierByNumber(phNumber));
					} else {
						logData.setMobileNumber(false);
					}
				}

				logsList.add(logData);

			}
			c.close();

			logsListData = logsList;
		}
	}

	private static String[] getContactIdAndName(String phoneNumber) {
		Uri uri = Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI,
				Uri.encode(phoneNumber));

		Cursor cursor = context.getContentResolver().query(uri,
				new String[] { PhoneLookup._ID, PhoneLookup.DISPLAY_NAME },
				null, null, null);

		String contactId = null;
		String contactName = null;

		if (cursor.moveToFirst()) {
			// do {

			// } while (cursor.moveToNext());
			contactId = cursor
					.getString(cursor.getColumnIndex(PhoneLookup._ID));
			contactName = cursor.getString(cursor
					.getColumnIndex(PhoneLookup.DISPLAY_NAME));
			// if(contactName!=null){
			// cursor.close();
			// return contactName;
			// }
		}

		cursor.close();

		String[] result = { contactId, contactName };

		return result;
	}

	private static Uri getContactPhotoUri(long contactId) {
		Uri contactUri = ContentUris.withAppendedId(Contacts.CONTENT_URI,
				contactId);
		Uri photoUri = Uri.withAppendedPath(contactUri,
				Contacts.Photo.CONTENT_DIRECTORY);
		// Try load the photo to check if it exists or not
		try {
			context.getContentResolver().openInputStream(photoUri);
		} catch (FileNotFoundException e) {
			photoUri = null;
		}
		return photoUri;
	}
	
	public static String getFormattedDuration(long seconds) {
		long minute = TimeUnit.SECONDS.toMinutes(seconds);
		long second = TimeUnit.SECONDS.toSeconds(seconds)
				- (TimeUnit.SECONDS.toMinutes(seconds) * 60);
		
		String strMinute, strSecond;
		strMinute = "" + minute;
		if(second < 10)
			strSecond = "0" + second;
		else
			strSecond = "" + second;
		
		String fomattedDuration = strMinute + ":" + strSecond;
		
		return fomattedDuration;
	}

}