package com.calllogsvisualizer.background;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.util.Log;


public class PhoneStateReceiver extends BroadcastReceiver {

	private static final String APPTAG = PhoneStateReceiver.class.getName();
		
	@Override
	public void onReceive(Context context, Intent intent) {
		String action = intent.getAction();
		if (action == TelephonyManager.ACTION_PHONE_STATE_CHANGED) {
			final String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
			if (state != null && !state.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
                return;
            }
			Log.i(APPTAG, "Call ended.");
		    Intent startServiceIntent = new Intent(context, LocationToDatabaseService.class);
		    context.startService(startServiceIntent);
		}

	}
	
}
