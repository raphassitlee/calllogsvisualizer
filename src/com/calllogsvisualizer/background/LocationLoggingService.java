package com.calllogsvisualizer.background;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import android.app.IntentService;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;

public class LocationLoggingService extends IntentService implements
	GoogleApiClient.ConnectionCallbacks,
	GoogleApiClient.OnConnectionFailedListener {

	private static final String APPTAG = LocationLoggingService.class.getName();
	public static final String FILENAME = "OutgoingCall.tmp";
	
	public LocationLoggingService() {
		super("LocationLoggerService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Log.i(APPTAG, "Service is started ");
		
		// Get phone number
		String phoneNumber = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);

		// Read location
		GoogleApiClient apiClient = new GoogleApiClient.Builder(this)
		        .addApi(LocationServices.API)
		        .addConnectionCallbacks(this)
		        .addOnConnectionFailedListener(this)
		        .build();
		Location location = null;		
		apiClient.connect();
		int i = 0;
		while (location == null && i < 30) {
			try {
				Thread.sleep(500);
				location = LocationServices.FusedLocationApi.getLastLocation(apiClient);
				i++;
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		apiClient.disconnect();

		writeTempFile(phoneNumber, location);
	}
		
	private void writeTempFile(String phoneNumber, Location location) {
		// Write to temporary file
	    File tempFile = null;
	    try {
	    	tempFile = new File(this.getApplicationContext().getFilesDir(), FILENAME);
			String path = tempFile.getAbsolutePath();
			Log.i(APPTAG, "Path = " + path);
			PrintWriter writer = new PrintWriter(tempFile);

			// write time
			Date date = new Date();
			String dateTime = String.valueOf(date.getTime());
			writer.println(dateTime);
			Log.i(APPTAG, "Time = " + dateTime);
			
			// write phone number
			writer.println(phoneNumber);
			Log.i(APPTAG, "Number = " + phoneNumber);
						
			// Write location if not null
			if (location != null) {
				String temp;
				
				temp = String.valueOf(location.getLatitude());
				writer.println(temp);
				Log.i(APPTAG, "Lat = " + temp);

				temp = String.valueOf(location.getLongitude());
				writer.println(temp);
				Log.i(APPTAG, "Long = " + temp);
			}
			writer.flush();
			writer.close();
			
			Log.i(APPTAG, "Write file success!.");
		} catch (IOException e) {
			Log.i(APPTAG, "Unable to create temporary OutgogingCall file.");
		}
	}

	// Override GoogleApiClient.ConnectionCallbacks
	
	@Override
	public void onConnected(Bundle bundle) {
		Log.i(APPTAG, "GoogleApiClient connection has been connected.");
	}

	@Override
	public void onConnectionSuspended(int code) {
		Log.i(APPTAG, "GoogleApiClient connection has been suspend.");
	}

	// Override GoogleApiClient.OnConnectionFailedListener
	
	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {
		Log.i(APPTAG, "GoogleApiClient connection has failed.");
	}
		
}
