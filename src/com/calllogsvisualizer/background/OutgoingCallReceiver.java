package com.calllogsvisualizer.background;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;


public class OutgoingCallReceiver extends BroadcastReceiver {

	private static final String APPTAG = OutgoingCallReceiver.class.getName();
	
	public static final String FILENAME = "OutgoingCall";
	
	@Override
	public void onReceive(Context context, Intent intent) {		
		String phoneNumber = getResultData();
	    if (phoneNumber == null) {
	      // No reformatted number, use the original
	      phoneNumber = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
	    }
	    
	    // Start Location Logger Service with phone number
	    Intent startServiceIntent = new Intent(context, LocationLoggingService.class);
	    startServiceIntent.putExtra(Intent.EXTRA_PHONE_NUMBER, phoneNumber);
		context.startService(startServiceIntent);
	}

}
