package com.calllogsvisualizer.background;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import com.calllogsvisualizer.orm.CallerLocation;

import android.app.IntentService;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.provider.CallLog;
import android.util.Log;

public class LocationToDatabaseService extends IntentService {

	private static final String APPTAG = LocationToDatabaseService.class.getName();
	public static final String FILENAME = "OutgoingCall.tmp";
	
	public LocationToDatabaseService() {
		super("LocationToDatabaseService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		ArrayList<String> lines = readTempFile(this.getApplicationContext());
	
		long dateTime = 0;
		String phoneNumber = null;		
		CallerLocation location = new CallerLocation();
		
		switch (lines.size()) {
		case 4:
			location.setLatitude(Double.parseDouble(lines.get(2)));
			location.setLongitude(Double.parseDouble(lines.get(3)));
		case 3:
		case 2:
			dateTime = Long.parseLong(lines.get(0));
			phoneNumber = lines.get(1);
		
			long rowId = getCallLogRowId(dateTime, phoneNumber);
			if (rowId != 0) {
				location.setCallLogId(rowId);
				location.save();
			}
			break;		
		}
		
	}
	
	private ArrayList<String> readTempFile(Context context) {
		ArrayList<String> lines = new ArrayList<String>();
		File tempFile = null;
	    try {
	    	tempFile = new File(context.getFilesDir(), FILENAME);
			BufferedReader reader = new BufferedReader(new FileReader(tempFile));

			String line;
			while ((line = reader.readLine()) != null) {
				lines.add(line);
			}
			
			reader.close();
			tempFile.delete();
		} catch (IOException e) {
			Log.i(APPTAG, "Unable to read temporary OutgogingCall file.");
		}
	    return lines;
	}
	
	private long getCallLogRowId(long dateTime, String phoneNumber) {
		String[] projection = { CallLog.Calls._ID, CallLog.Calls.DATE, CallLog.Calls.NUMBER };
		String selection = "(" + 
				CallLog.Calls.NUMBER + " = ?" + " AND " +
				CallLog.Calls.TYPE + " = ?" +
				")";
		String[] selectionArgs = { 
				phoneNumber,
				String.valueOf(CallLog.Calls.OUTGOING_TYPE)
				};
		String sortOrder = CallLog.Calls.DATE + " DESC";
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			
		}
		
		ContentResolver resolver = this.getApplicationContext().getContentResolver();
		Cursor cursor = resolver.query(CallLog.Calls.CONTENT_URI, 
				projection, selection, selectionArgs, sortOrder);
		if (cursor.moveToNext()) {
			long logRowId = cursor.getLong(cursor.getColumnIndex(CallLog.Calls._ID));
			long logDate = cursor.getLong(cursor.getColumnIndex(CallLog.Calls.DATE));
			String logNumber = cursor.getString(cursor.getColumnIndex(CallLog.Calls.NUMBER));
			Log.i(APPTAG, "Row Id = " + logRowId);
			Log.i(APPTAG, "Date = " + (new Date(logDate)).toString());
			Log.i(APPTAG, "Number = " + logNumber);
			
			if (logDate - 1000 < dateTime || dateTime < logDate + 1000) {
				return logRowId;
			}
		}
		cursor.close();
		return 0;
	}

}
