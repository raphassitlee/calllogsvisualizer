package com.calllogsvisualizer.orm;

import java.util.Date;
import java.util.List;

import android.provider.BaseColumns;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

@Table(name = "DailyCarrierSummaries", id = BaseColumns._ID)
public class DailyCarrierSummary extends Model {

	@Column(name = "Date", notNull = true)
	private long date;
	
	@Column(name = "CarrierId", notNull = true)
	private long carrierId;
	
	@Column(name = "Duration", notNull = true)
	private long duration;

	public DailyCarrierSummary() {
		super();
	}

	public DailyCarrierSummary(long date, long carrierId, long duration) {
		super();
		this.date = date;
		this.carrierId = carrierId;
		this.duration = duration;
	}
	
	public static List<DailyCarrierSummary> getByDate(long date) {
		return new Select()
				.from(DailyCarrierSummary.class)
				.where("Date = ?", date)
				.execute();
	}
	
	public static List<DailyCarrierSummary> getByDate(Date date) {
		return new Select()
				.from(DailyCarrierSummary.class)
				.where("Date = ?", date.getTime())
				.execute();
	}
	
	public static List<DailyCarrierSummary> getByDateRange(long startDate, long endDate) {
		return new Select("_id, Date, CarrierId, SUM(Duration) AS Duration")
				.from(DailyCarrierSummary.class)
				.where("Date >= ? AND Date <= ?", startDate, endDate)
				.groupBy("CarrierId")
				.execute();
	}
	
	public static DailyCarrierSummary getByDateAndCarrierId(long date, long carrierId) {
		return new Select()
				.from(DailyCarrierSummary.class)
				.where("Date = ? AND CarrierId = ?", date, carrierId)
				.executeSingle();
	}
	
	public static DailyCarrierSummary getLatestByDate() {
		return new Select()
				.from(DailyCarrierSummary.class)
				.orderBy("Date DESC")
				.executeSingle();
	}
	
	public static DailyCarrierSummary getOldestByDate() {
		return new Select()
				.from(DailyCarrierSummary.class)
				.orderBy("Date")
				.executeSingle();
	}
	
	public static void deleteAll() {
		new Delete().from(DailyCarrierSummary.class).execute();
	}

	public long getDate() {
		return date;
	}

	public void setDate(long date) {
		this.date = date;
	}

	public long getCarrierId() {
		return carrierId;
	}

	public void setCarrierId(long carrierId) {
		this.carrierId = carrierId;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}
	
}
