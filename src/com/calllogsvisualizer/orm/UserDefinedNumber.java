package com.calllogsvisualizer.orm;

import java.util.List;

import android.provider.BaseColumns;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

@Table(name = "UserDefinedNumbers", id = BaseColumns._ID)
public class UserDefinedNumber extends Model {
	
	@Column(name = "Number", notNull = true, unique = true)
	private String number;
	
	@Column(name = "CarrierId", notNull = true)
	private long carrierId;

	public UserDefinedNumber() {
		super();
	}

	public UserDefinedNumber(String number, long carrierId) {
		super();
		this.number = number;
		this.carrierId = carrierId;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public long getCarrierId() {
		return carrierId;
	}

	public void setCarrierId(long carrierId) {
		this.carrierId = carrierId;
	}
	
	public static List<UserDefinedNumber> getAll() {
		return new Select()
			.from(UserDefinedNumber.class)
			.execute();
	}

	public static UserDefinedNumber getByNumber(String number) {
		return new Select()
			.from(UserDefinedNumber.class)
			.where("Number = ?", number)
			.executeSingle();
	}
}
