package com.calllogsvisualizer.orm;

import android.provider.BaseColumns;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

@Table(name = "CallerLocations", id = BaseColumns._ID)
public class CallerLocation extends Model {
	
	@Column(name = "CallLogId", notNull = true)
	private long callLogId;
	
	@Column(name = "Latitude")
	private double latitude;
	
	@Column(name = "Longitude")
	private double longitude;
	
	public CallerLocation() {
		super();
	}	

	public long getCallLogId() {
		return callLogId;
	}

	public void setCallLogId(long callLogId) {
		this.callLogId = callLogId;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	
	public static CallerLocation findLocationByCallLogId(long callLogId) {
		return new Select()
			.from(CallerLocation.class)
			.where("CallLogId = ?", callLogId)
			.executeSingle();
	}

}
