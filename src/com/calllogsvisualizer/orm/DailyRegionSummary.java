package com.calllogsvisualizer.orm;

import java.util.Date;
import java.util.List;

import android.provider.BaseColumns;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

@Table(name = "DailyRegionSummaries", id = BaseColumns._ID)
public class DailyRegionSummary extends Model {

	@Column(name = "Date", notNull = true)
	private long date;
	
	@Column(name = "International", notNull = true)
	private boolean international;
	
	@Column(name = "Duration", notNull = true)
	private long duration;

	public DailyRegionSummary() {
		super();
	}

	public DailyRegionSummary(long date, boolean international, long duration) {
		super();
		this.date = date;
		this.international = international;
		this.duration = duration;
	}

	public static List<DailyRegionSummary> getByDate(long date) {
		return new Select()
				.from(DailyRegionSummary.class)
				.where("Date = ?", date)
				.execute();
	}
	
	public static List<DailyRegionSummary> getByDate(Date date) {
		return new Select()
				.from(DailyRegionSummary.class)
				.where("Date = ?", date.getTime())
				.execute();
	}
	
	public static List<DailyRegionSummary> getByDateRange(long startDate, long endDate) {
		return new Select("_id, Date, International, SUM(Duration) AS Duration")
				.from(DailyRegionSummary.class)
				.where("Date >= ? AND Date <= ?", startDate, endDate)
				.groupBy("International")
				.execute();
	}
	
	public static DailyRegionSummary getByDateAndInternational(long date, boolean international) {
		return new Select()
				.from(DailyRegionSummary.class)
				.where("Date = ? AND International = ?", date, international)
				.executeSingle();
	}
	
	public static DailyRegionSummary getLatestByDate() {
		return new Select()
				.from(DailyRegionSummary.class)
				.orderBy("Date DESC")
				.executeSingle();
	}
	
	public static DailyRegionSummary getOldestByDate() {
		return new Select()
				.from(DailyRegionSummary.class)
				.orderBy("Date")
				.executeSingle();
	}

	public long getDate() {
		return date;
	}

	public void setDate(long date) {
		this.date = date;
	}

	public boolean isInternational() {
		return international;
	}

	public void setInternational(boolean international) {
		this.international = international;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}
	
}
