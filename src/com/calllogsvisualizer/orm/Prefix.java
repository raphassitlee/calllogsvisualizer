package com.calllogsvisualizer.orm;

import java.util.List;

import android.provider.BaseColumns;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

@Table(name = "Prefixes", id = BaseColumns._ID)
public class Prefix extends Model {
			
	@Column(name = "NumberPrefix", notNull = true, unique = true)
	private String numberPrefix;
	
	@Column(name = "CarrierId", notNull = true)
	private long carrierId;
	
	public Prefix() {
		super();
	}
	
	public Prefix(String numberPrefix, int carrierId) {
		super();
		this.numberPrefix = numberPrefix;
		this.carrierId = carrierId;
	}
	
	public String getNumberPrefix() {
		return numberPrefix;
	}

	public long getCarrierId() {
		return carrierId;
	}
	
	public String getCarrierName() {
		return Carrier.findById(carrierId).getName();
	}
	
	public static Prefix findByNumberPrefix(String numberPrefix) {
		return new Select()
			.from(Prefix.class)
			.where("NumberPrefix = ?", numberPrefix)
			.executeSingle();
	}
	
	public static List<Prefix> findAll() {
		return new Select()
			.from(Prefix.class)
			.orderBy("NumberPrefix ASC")
			.execute();
	}
		
}
