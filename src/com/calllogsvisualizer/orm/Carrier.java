package com.calllogsvisualizer.orm;

import java.util.List;

import android.provider.BaseColumns;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

@Table(name = "Carriers", id = BaseColumns._ID)
public class Carrier extends Model {
	
	@Column(name = "Name", notNull = true, unique = true)
	private String name;
		
	public Carrier() {
		super();
	}
	
	public Carrier(String name) {
		super();
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

	public List<Prefix> getPrefixes() {
		return getMany(Prefix.class, "CarrierId");
	}
	
	public static Carrier findById(long Id) {
		return new Select()
			.from(Carrier.class)
			.where(BaseColumns._ID + " = ?", Id)
			.executeSingle();
	}
	
	public static Carrier findByName(String name) {
		return new Select()
			.from(Carrier.class)
			.where("Name = ?", name)
			.executeSingle();
	}
	
	public static List<Carrier> findAll() {
		return new Select()
			.from(Carrier.class)
			.orderBy(BaseColumns._ID + " ASC")
			.execute();
	}
	
}
