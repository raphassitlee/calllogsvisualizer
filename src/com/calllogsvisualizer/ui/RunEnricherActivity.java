package com.calllogsvisualizer.ui;

import com.calllogsvisualizer.R;
import com.calllogsvisualizer.background.LocationLoggingService;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;


public class RunEnricherActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.run_enricher);
	}
	
	public void onButtonClick(View view) {
		Intent intent = new Intent(this, LocationLoggingService.class);
		this.startService(intent);
	}
}
