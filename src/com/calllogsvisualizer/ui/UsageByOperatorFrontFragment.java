package com.calllogsvisualizer.ui;

import java.util.List;

import com.calllogsvisualizer.R;
import com.calllogsvisualizer.orm.DailyCarrierSummary;
import com.calllogsvisualizer.orm.DailyRegionSummary;
import com.calllogsvisualizer.util.CallLogsUtil;
import com.calllogsvisualizer.util.UsageUtil;

import android.app.Fragment;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class UsageByOperatorFrontFragment extends Fragment {

	private static final int RED = Color.parseColor("#f03131");
	private static final int ORANGE = Color.parseColor("#ffae18");
	private static final int GREEN = Color.parseColor("#83b600");
	private static final int AQUA = Color.parseColor("#16a5d7");
	private static final int GREY = Color.parseColor("#999999");
	
	private static final int PURPLE = Color.parseColor("#c841ea");
	private static final int YELLOW = Color.parseColor("#eae825");

	private View rootView;

//	private static ArrayList<RelativeLayout> operatorBarItems = new ArrayList<RelativeLayout>();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_usage_operator_front,
				container, false);

		initBarCharts();

		return rootView;
	}

	private void initBarCharts() {

		// ADD TODAY'S USAGE
//		ArrayList<LogsListData> today = CallLogsUtil.getTodayCallDetails(getActivity().getApplicationContext());
//		
//		long todayDurationDomestic = 0;
//		long todayDurationInternational = 0;
//		long todayDurationOptus = 0;
//		long todayDurationVodafone = 0;
//		long todayDurationTelstra = 0;
//		long todayDurationLycamobile = 0;
//		
//		for (int i = 0; i < today.size(); i++) {
//			if(today.get(i).isInternational()) {
//				todayDurationInternational += Long.parseLong(today.get(i).getDuration());
//			} else {
//				todayDurationDomestic += Long.parseLong(today.get(i).getDuration());
//				if(today.get(i).isMobileNumber()) {
//					if (today.get(i).getOperator().getId() == 1)
//						todayDurationTelstra += Long.parseLong(today.get(i).getDuration());
//					else if (today.get(i).getOperator().getId() == 2)
//						todayDurationOptus += Long.parseLong(today.get(i).getDuration());
//					else if (today.get(i).getOperator().getId() == 3)
//						todayDurationVodafone += Long.parseLong(today.get(i).getDuration());
//					else if (today.get(i).getOperator().getId() == 4)
//						todayDurationLycamobile += Long.parseLong(today.get(i).getDuration());
//				}
//			}
//		}
		
		TodayDurations todayDurations = CallLogsUtil.getTodayCallDetailDurations(getActivity().getApplicationContext());
		
		Long[] datePair = UsageUtil.getThisMonthDates();
		
		long durationDomestic = todayDurations.getTodayDurationDomestic();
		long durationInternational = todayDurations.getTodayDurationInternational();
		long durationOptus = todayDurations.getTodayDurationOptus();
		long durationVodafone = todayDurations.getTodayDurationVodafone();
		long durationTelstra = todayDurations.getTodayDurationTelstra();
		long durationLycamobile = todayDurations.getTodayDurationLycamobile();
		
		// Regions
		List<DailyRegionSummary> dataRegion = DailyRegionSummary.getByDateRange(datePair[0], datePair[1]);
		
		if(dataRegion.size() == 0) {
			rootView.findViewById(R.id.noUsageByRegionTextView).setVisibility(View.VISIBLE);
		} else {
		
			long totalDuration = durationDomestic + durationInternational;
			
//			DailyRegionSummary dataDomestic = null, dataInternational = null;
			
			for (int i = 0; i < dataRegion.size(); i++) {
				if(dataRegion.get(i).isInternational()) {
//					dataInternational = dataRegion.get(i);
					durationInternational += dataRegion.get(i).getDuration();
				} else {
//					dataDomestic = dataRegion.get(i);
					durationDomestic += dataRegion.get(i).getDuration();
				}
				totalDuration += dataRegion.get(i).getDuration();
			}
			
			RelativeLayout item;
			TextView tvOperatorName, tvDuration;
			ProgressBar progressBar;
			
			if(durationDomestic > 0){
				item = (RelativeLayout) rootView.findViewById(R.id.domestic);
				item.setVisibility(View.VISIBLE);
		
				tvOperatorName = (TextView) item.findViewById(R.id.domesticText);
				tvOperatorName.setText("Domestic");
				tvDuration = (TextView) item.findViewById(R.id.durationDomestic);
				tvDuration.setText(CallLogsUtil.getFormattedDuration(durationDomestic)+" min");
				
				progressBar = (ProgressBar) item.findViewById(R.id.progressBarDomestic);
				if(totalDuration != 0) {
					progressBar.setProgress(Math.round((float)durationDomestic / totalDuration * 100));
				}
				setBarColor(progressBar, PURPLE);
			}
			if(durationInternational > 0) {
				item = (RelativeLayout) rootView.findViewById(R.id.international);
				item.setVisibility(View.VISIBLE);
		
				tvOperatorName = (TextView) item.findViewById(R.id.internationalText);
				tvOperatorName.setText("International");
				tvDuration = (TextView) item.findViewById(R.id.durationInternational);
				tvDuration.setText(CallLogsUtil.getFormattedDuration(durationInternational)+" min");
				
				progressBar = (ProgressBar) item.findViewById(R.id.progressBarInternational);
				if(totalDuration != 0) {
					progressBar.setProgress(Math.round((float)durationInternational / totalDuration * 100));
				}
				setBarColor(progressBar, YELLOW);
			}
		}
		
		List<DailyCarrierSummary> data = DailyCarrierSummary.getByDateRange(datePair[0], datePair[1]);
		
		long totalDuration = durationOptus + durationTelstra + durationVodafone + durationLycamobile;
		
		if(data.size() == 0 && totalDuration == 0) {
			if(durationDomestic > 0) {
				RelativeLayout item = (RelativeLayout) rootView.findViewById(R.id.operator5);
				item.setVisibility(View.VISIBLE);
	
				TextView tvOperatorName = (TextView) item.findViewById(R.id.operatorName5);
				tvOperatorName.setText("Other");
				TextView tvDuration = (TextView) item.findViewById(R.id.duration5);
				tvDuration.setText(CallLogsUtil.getFormattedDuration(durationDomestic)+" min");
				
				ProgressBar progressBar = (ProgressBar) item.findViewById(R.id.progressBar5);
				progressBar.setProgress(100);
				setBarColor(progressBar, GREY);
			} else {
				rootView.findViewById(R.id.noUsageByOperatorTextView).setVisibility(View.VISIBLE);
			}
		} else {
			
//			DailyCarrierSummary dataOptus = null, dataVodafone = null, dataTelstra = null, dataLycamobile = null;
		
			for (int i = 0; i < data.size(); i++) {
				if (data.get(i).getCarrierId() == 1) {
//					dataTelstra = data.get(i);
					durationTelstra += data.get(i).getDuration();
				} else if (data.get(i).getCarrierId() == 2) {
//					dataOptus = data.get(i);
					durationOptus += data.get(i).getDuration();
				} else if (data.get(i).getCarrierId() == 3) {
//					dataVodafone = data.get(i);
					durationVodafone += data.get(i).getDuration();
				} else if (data.get(i).getCarrierId() == 4) {
//					dataLycamobile = data.get(i);
					durationLycamobile += data.get(i).getDuration();
				}
				totalDuration += data.get(i).getDuration();
			}
			
			totalDuration = totalDuration  + (durationDomestic - totalDuration);
			
			RelativeLayout item;
			TextView tvOperatorName, tvDuration;
			ProgressBar progressBar;
			
			if(durationOptus > 0) {
				item = (RelativeLayout) rootView.findViewById(R.id.operator1);
				item.setVisibility(View.VISIBLE);
	
				tvOperatorName = (TextView) item.findViewById(R.id.operatorName1);
				tvOperatorName.setText("Optus");
				tvDuration = (TextView) item.findViewById(R.id.duration1);
				tvDuration.setText(CallLogsUtil.getFormattedDuration(durationOptus)+" min");
				
				progressBar = (ProgressBar) item.findViewById(R.id.progressBar1);
				if(totalDuration != 0) {
					progressBar.setProgress(Math.round((float)durationOptus / totalDuration * 100));
				}
				setBarColor(progressBar, ORANGE);
			}
			if(durationVodafone > 0) {
				item = (RelativeLayout) rootView.findViewById(R.id.operator2);
				item.setVisibility(View.VISIBLE);
	
				tvOperatorName = (TextView) item.findViewById(R.id.operatorName2);
				tvOperatorName.setText("Vodafone");
				tvDuration = (TextView) item.findViewById(R.id.duration2);
				tvDuration.setText(CallLogsUtil.getFormattedDuration(durationVodafone)+" min");
				
				progressBar = (ProgressBar) item.findViewById(R.id.progressBar2);
				if(totalDuration != 0) {
					progressBar.setProgress(Math.round((float)durationVodafone / totalDuration * 100));
				}
				setBarColor(progressBar, RED);
			}
			if(durationTelstra > 0) {
				item = (RelativeLayout) rootView.findViewById(R.id.operator3);
				item.setVisibility(View.VISIBLE);
	
				tvOperatorName = (TextView) item.findViewById(R.id.operatorName3);
				tvOperatorName.setText("Telstra");
				tvDuration = (TextView) item.findViewById(R.id.duration3);
				tvDuration.setText(CallLogsUtil.getFormattedDuration(durationTelstra)+" min");
				
				progressBar = (ProgressBar) item.findViewById(R.id.progressBar3);
				if(totalDuration != 0) {
					progressBar.setProgress(Math.round((float)durationTelstra / totalDuration * 100));
				}
				setBarColor(progressBar, AQUA);
			}
			if(durationLycamobile > 0) {
				item = (RelativeLayout) rootView.findViewById(R.id.operator4);
				item.setVisibility(View.VISIBLE);
	
				tvOperatorName = (TextView) item.findViewById(R.id.operatorName4);
				tvOperatorName.setText("Lycamobile");
				tvDuration = (TextView) item.findViewById(R.id.duration4);
				tvDuration.setText(CallLogsUtil.getFormattedDuration(durationLycamobile)+" min");
				
				progressBar = (ProgressBar) item.findViewById(R.id.progressBar4);
				if(totalDuration != 0) {
					progressBar.setProgress(Math.round((float)durationLycamobile / totalDuration * 100));
				}
				setBarColor(progressBar, GREEN);
			}
			if(durationDomestic - totalDuration > 0) {
				item = (RelativeLayout) rootView.findViewById(R.id.operator5);
				item.setVisibility(View.VISIBLE);
	
				tvOperatorName = (TextView) item.findViewById(R.id.operatorName5);
				tvOperatorName.setText("Other");
				tvDuration = (TextView) item.findViewById(R.id.duration5);
				tvDuration.setText(CallLogsUtil.getFormattedDuration(durationDomestic - totalDuration)+" min");
				
				progressBar = (ProgressBar) item.findViewById(R.id.progressBar5);
				if(totalDuration != 0) {
					progressBar.setProgress(Math.round((float)(durationDomestic - totalDuration) / totalDuration * 100));
				}
				setBarColor(progressBar, GREY);
			}
		}
		
		
	}
	
	private void setBarColor(ProgressBar progressBar, int color) {
		Resources res = getResources();
		Rect bounds = progressBar.getProgressDrawable().getBounds();

		int drawableId = R.drawable.usage_operator_bar_aqua;
		
		if(color == RED)
			drawableId = R.drawable.usage_operator_bar_red;
		else if (color == ORANGE)
			drawableId = R.drawable.usage_operator_bar_orange;
		else if (color == GREEN)
			drawableId = R.drawable.usage_operator_bar_green;
		else if (color == AQUA)
			drawableId = R.drawable.usage_operator_bar_aqua;
		else if (color == GREY)
			drawableId = R.drawable.usage_operator_bar_grey;
		else if (color == PURPLE)
			drawableId = R.drawable.usage_operator_bar_purple;
		else if (color == YELLOW)
			drawableId = R.drawable.usage_operator_bar_yellow;
			
		progressBar.setProgressDrawable(res
				.getDrawable(drawableId));

		progressBar.getProgressDrawable().setBounds(bounds);
	}

}
