package com.calllogsvisualizer.ui;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import com.calllogsvisualizer.R;
import com.calllogsvisualizer.R.id;
import com.calllogsvisualizer.R.layout;
import com.calllogsvisualizer.orm.Carrier;
import com.calllogsvisualizer.orm.UserDefinedNumber;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class UserDefinedBaseAdapter extends BaseAdapter {
	
	ArrayList<UserDefinedNumber> userDefinedList = new ArrayList<UserDefinedNumber>();
	LayoutInflater inflater;
    Context context;

	public UserDefinedBaseAdapter(ArrayList<UserDefinedNumber> userDefinedList, Context context) {
		super();
		this.userDefinedList = userDefinedList;
		this.context = context;
		inflater = LayoutInflater.from(this.context);
	}

	@Override
	public int getCount() {
		return userDefinedList.size();
	}

	@Override
	public Object getItem(int position) {
		return userDefinedList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return userDefinedList.get(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder mViewHolder;
        
        if(convertView == null) {
                convertView = inflater.inflate(R.layout.user_defined_list_item, null);
                mViewHolder = new ViewHolder();
                convertView.setTag(mViewHolder);
        } else {
                mViewHolder = (ViewHolder) convertView.getTag();
        }
        
		mViewHolder.number = detail(convertView, R.id.number,
				userDefinedList.get(position).getNumber());
		
		mViewHolder.operator = detail(convertView, R.id.operator,
				Carrier.findById(userDefinedList.get(position).getCarrierId()).getName());

        return convertView;
	}
	
	private TextView detail(View v, int resId, String text) {
        TextView tv = (TextView) v.findViewById(resId);
        tv.setText(text);
        return tv;
	}

	private class ViewHolder {
		TextView number, operator;
	}

}
