package com.calllogsvisualizer.ui;

import java.util.List;

import com.androidplot.pie.PieChart;
import com.androidplot.pie.Segment;
import com.androidplot.pie.SegmentFormatter;
import com.calllogsvisualizer.R;
import com.calllogsvisualizer.orm.DailyCarrierSummary;
import com.calllogsvisualizer.orm.DailyRegionSummary;
import com.calllogsvisualizer.util.CallLogsUtil;
import com.calllogsvisualizer.util.UsageUtil;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class UsageByOperatorBackFragment extends Fragment {

	private PieChart operatorsPie, regionsPie;
	
	private View rootView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_usage_operator_back,
				container, false);

		initPieChart();

		return rootView;
	}
	
	private void initPieChart() {
		SegmentFormatter sf1 = new SegmentFormatter();
		sf1.configure(rootView.getContext(), R.xml.pie_segment_formatter_red);

		sf1.getFillPaint();

		SegmentFormatter sf2 = new SegmentFormatter();
		sf2.configure(rootView.getContext(), R.xml.pie_segment_formatter_orange);

		sf2.getFillPaint();

		SegmentFormatter sf3 = new SegmentFormatter();
		sf3.configure(rootView.getContext(), R.xml.pie_segment_formatter_green);

		sf3.getFillPaint();

		SegmentFormatter sf4 = new SegmentFormatter();
		sf4.configure(rootView.getContext(), R.xml.pie_segment_formatter_aqua);

		sf4.getFillPaint();
		
		SegmentFormatter sf5 = new SegmentFormatter();
		sf5.configure(rootView.getContext(), R.xml.pie_segment_formatter_grey);

		sf5.getFillPaint();
		
		SegmentFormatter sf6 = new SegmentFormatter();
		sf6.configure(rootView.getContext(), R.xml.pie_segment_formatter_purple);

		sf6.getFillPaint();
		
		SegmentFormatter sf7 = new SegmentFormatter();
		sf7.configure(rootView.getContext(), R.xml.pie_segment_formatter_yellow);

		sf7.getFillPaint();
		
		// ADD TODAY'S USAGE
//		ArrayList<LogsListData> today = CallLogsUtil.getTodayCallDetails(getActivity().getApplicationContext());
//		
//		long todayDurationDomestic = 0;
//		long todayDurationInternational = 0;
//		long todayDurationOptus = 0;
//		long todayDurationVodafone = 0;
//		long todayDurationTelstra = 0;
//		long todayDurationLycamobile = 0;
//		
//		for (int i = 0; i < today.size(); i++) {
//			if(today.get(i).isInternational()) {
//				todayDurationInternational += Long.parseLong(today.get(i).getDuration());
//			} else {
//				todayDurationDomestic += Long.parseLong(today.get(i).getDuration());
//				if(today.get(i).isMobileNumber()) {
//					if (today.get(i).getOperator().getId() == 1)
//						todayDurationTelstra += Long.parseLong(today.get(i).getDuration());
//					else if (today.get(i).getOperator().getId() == 2)
//						todayDurationOptus += Long.parseLong(today.get(i).getDuration());
//					else if (today.get(i).getOperator().getId() == 3)
//						todayDurationVodafone += Long.parseLong(today.get(i).getDuration());
//					else if (today.get(i).getOperator().getId() == 4)
//						todayDurationLycamobile += Long.parseLong(today.get(i).getDuration());
//				}
//			}
//		}
		
		Long[] datePair = UsageUtil.getThisMonthDates();
		
		TodayDurations todayDurations = CallLogsUtil.getTodayCallDetailDurations(getActivity().getApplicationContext());
		
		long durationDomestic = todayDurations.getTodayDurationDomestic();
		long durationInternational = todayDurations.getTodayDurationInternational();
		long durationOptus = todayDurations.getTodayDurationOptus();
		long durationVodafone = todayDurations.getTodayDurationVodafone();
		long durationTelstra = todayDurations.getTodayDurationTelstra();
		long durationLycamobile = todayDurations.getTodayDurationLycamobile();
				
		// region
		
		List<DailyRegionSummary> dataRegion = DailyRegionSummary.getByDateRange(datePair[0], datePair[1]);
		
		if(dataRegion.size() > 0){
//		DailyRegionSummary dataDomestic = null, dataInternational = null;
		
		for (int i = 0; i < dataRegion.size(); i++) {
			if(dataRegion.get(i).isInternational()) {
//				dataInternational = dataRegion.get(i);
				durationInternational += dataRegion.get(i).getDuration();
			} else {
//				dataDomestic = dataRegion.get(i);
				durationDomestic += dataRegion.get(i).getDuration();
			}
		}
		
		regionsPie = (PieChart) rootView.findViewById(R.id.regionsPieChart);
		
		if (durationDomestic > 0) {
			Segment domestic = new Segment("Domestic",
					durationDomestic);
			regionsPie.addSeries(domestic, sf6);
		}
		if (durationInternational > 0) {
			Segment international = new Segment("International",
					durationInternational);
			regionsPie.addSeries(international, sf7);
		}
		
		regionsPie.getBorderPaint().setColor(Color.TRANSPARENT);
		regionsPie.getBackgroundPaint().setColor(Color.TRANSPARENT);
		} else {
			rootView.findViewById(R.id.noUsageByRegionTextView).setVisibility(View.VISIBLE);
		}
		
		// operator
		
		List<DailyCarrierSummary> data = DailyCarrierSummary.getByDateRange(datePair[0], datePair[1]);
		
		operatorsPie = (PieChart) rootView.findViewById(R.id.operatorsPieChart);
		
		long totalOperatorDuration = durationTelstra + durationOptus + durationVodafone + durationLycamobile;
		
		if(data.size() == 0 && totalOperatorDuration == 0) {
			if (durationDomestic > 0) {
				Segment s5 = new Segment("Other", durationDomestic);
				operatorsPie.addSeries(s5, sf5);
			} else {
				rootView.findViewById(R.id.noUsageByOperatorTextView).setVisibility(View.VISIBLE);
			}
		} else {
//		DailyCarrierSummary dataOptus = null, dataVodafone = null, dataTeltra = null, dataLycamobile = null;

		for (int i = 0; i < data.size(); i++) {
			if (data.get(i).getCarrierId() == 1) {
//				dataTelstra = data.get(i);
				durationTelstra += data.get(i).getDuration();
			} else if (data.get(i).getCarrierId() == 2) {
//				dataOptus = data.get(i);
				durationOptus += data.get(i).getDuration();
			} else if (data.get(i).getCarrierId() == 3) {
//				dataVodafone = data.get(i);
				durationVodafone += data.get(i).getDuration();
			} else if (data.get(i).getCarrierId() == 4) {
//				dataLycamobile = data.get(i);
				durationLycamobile += data.get(i).getDuration();
			}
			totalOperatorDuration += data.get(i).getDuration();
		}

		if (durationVodafone > 0) {
			Segment s1 = new Segment("Vodafone", durationVodafone);
			operatorsPie.addSeries(s1, sf1);
		}
		if (durationOptus > 0) {
			Segment s2 = new Segment("Optus", durationOptus);
			operatorsPie.addSeries(s2, sf2);
		}
		if (durationLycamobile > 0) {
			Segment s3 = new Segment("Lycamobile", durationLycamobile);
			operatorsPie.addSeries(s3, sf3);
		}
		if (durationTelstra > 0) {
			Segment s4 = new Segment("Telstra", durationTelstra);
			operatorsPie.addSeries(s4, sf4);
		}
		if (durationDomestic > 0) {
			if (durationDomestic - totalOperatorDuration > 0) {
				Segment s5 = new Segment("Other", durationDomestic - totalOperatorDuration);
				operatorsPie.addSeries(s5, sf5);
			}
		}
		}
		
		operatorsPie.getBorderPaint().setColor(Color.TRANSPARENT);
		operatorsPie.getBackgroundPaint().setColor(Color.TRANSPARENT);
	}

}
