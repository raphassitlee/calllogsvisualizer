package com.calllogsvisualizer.ui;

import com.calllogsvisualizer.R;
import com.calllogsvisualizer.util.UsageUtil;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * A fragment that launches other parts of the demo application.
 */
public class UsageSectionFragment extends Fragment {
	
	private Handler mHandler = new Handler();
	private boolean mShowingBack = false;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		final View rootView = inflater.inflate(
				R.layout.fragment_section_usage, container, false);
		
		if(getFragmentManager().getBackStackEntryCount() > 0)
    		getFragmentManager().popBackStack();
		
		if (savedInstanceState == null) {
            // If there is no saved instance state, add a fragment representing the
            // front of the card to this activity. If there is saved instance state,
            // this fragment will have already been added to the activity.
            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment, new UsageByOperatorFrontFragment())
                    .commit();
            mShowingBack = false;
        } else {
            mShowingBack = (getFragmentManager().getBackStackEntryCount() > 0);
        }
		
		rootView.findViewById(R.id.fragment).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						flipCard();
					}
				});

		rootView.findViewById(R.id.previous_cycles_button)
				.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						Intent intent = new Intent(getActivity(),
								PreviousCyclesActivity.class);
						startActivity(intent);
					}
				});

		return rootView;
	}
	
	private void flipCard() {
        if (mShowingBack) {
            getFragmentManager().popBackStack();
            mShowingBack = false;
            return;
        }

        // Flip to the back.

        mShowingBack = true;

        // Create and commit a new fragment transaction that adds the fragment for the back of
        // the card, uses custom animations, and is part of the fragment manager's back stack.

        getFragmentManager()
                .beginTransaction()

                // Replace the default fragment animations with animator resources representing
                // rotations when switching to the back of the card, as well as animator
                // resources representing rotations when flipping back to the front (e.g. when
                // the system Back button is pressed).
                .setCustomAnimations(
                        R.animator.card_flip_right_in, R.animator.card_flip_right_out,
                        R.animator.card_flip_left_in, R.animator.card_flip_left_out)

                // Replace any fragments currently in the container view with a fragment
                // representing the next page (indicated by the just-incremented currentPage
                // variable).
                .replace(R.id.fragment, new UsageByOperatorBackFragment())

                // Add this transaction to the back stack, allowing users to press Back
                // to get to the front of the card.
                .addToBackStack(null)

                // Commit the transaction.
                .commit();

        // Defer an invalidation of the options menu (on modern devices, the action bar). This
        // can't be done immediately because the transaction may not yet be committed. Commits
        // are asynchronous in that they are posted to the main thread's message loop.
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                
            }
        });
    }
	
}
