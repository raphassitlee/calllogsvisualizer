package com.calllogsvisualizer.ui;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.calllogsvisualizer.R;
import com.calllogsvisualizer.util.UsageUtil;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.view.ViewPager;
import android.view.MenuItem;

public class PreviousCyclesActivity extends Activity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide fragments representing
     * each object in a collection. We use a {@link android.support.v4.app.FragmentStatePagerAdapter}
     * derivative, which will destroy and re-create fragments as needed, saving and restoring their
     * state in the process. This is important to conserve memory and is a best practice when
     * allowing navigation between objects in a potentially large collection.
     */
    PreviousCyclesCollectionPagerAdapter mPreviousCyclesCollectionPagerAdapter;

    /**
     * The {@link android.support.v4.view.ViewPager} that will display the object collection.
     */
    ViewPager mViewPager;
    
    private static List<Long[]> datePairs;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_previous_cycles);
        
//        SharedPreferences settings = getSharedPreferences("pref_cycle_start_date", 0);
//		int cycleStartDate = settings.getInt("cycleStartDate", 1);
		
		datePairs = UsageUtil.getMonthDates();
		Collections.reverse(datePairs);

        // Create an adapter that when requested, will return a fragment representing an object in
        // the collection.
        // 
        // ViewPager and its adapters use support library fragments, so we must use
        // getSupportFragmentManager.
        mPreviousCyclesCollectionPagerAdapter = new PreviousCyclesCollectionPagerAdapter(getFragmentManager());

        // Set up action bar.
        final ActionBar actionBar = getActionBar();

        // Specify that the Home button should show an "Up" caret, indicating that touching the
        // button will take the user one step up in the application's hierarchy.
        actionBar.setDisplayHomeAsUpEnabled(true);

        // Set up the ViewPager, attaching the adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mPreviousCyclesCollectionPagerAdapter);
        mViewPager.setCurrentItem(mPreviousCyclesCollectionPagerAdapter.getCount() - 1);
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        // Respond to the action bar's Up/Home button
        case android.R.id.home:
            Intent upIntent = NavUtils.getParentActivityIntent(this);
            if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                // This activity is NOT part of this app's task, so create a new task
                // when navigating up, with a synthesized back stack.
                TaskStackBuilder.create(this)
                        // Add all of this activity's parents to the back stack
                        .addNextIntentWithParentStack(upIntent)
                        // Navigate up to the closest parent
                        .startActivities();
            } else {
                // This activity is part of this app's task, so simply
                // navigate up to the logical parent activity.
                NavUtils.navigateUpTo(this, upIntent);
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A {@link android.support.v4.app.FragmentStatePagerAdapter} that returns a fragment
     * representing an object in the collection.
     */
    public static class PreviousCyclesCollectionPagerAdapter extends FragmentStatePagerAdapter {

        public PreviousCyclesCollectionPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            PreviousCycleFragment fragment = new PreviousCycleFragment();
            Bundle args = new Bundle();
            args.putLong(PreviousCycleFragment.ARG_START_DATE, datePairs.get(i)[0]);
            args.putLong(PreviousCycleFragment.ARG_END_DATE, datePairs.get(i)[1]);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public int getCount() {
            return datePairs.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
        	Calendar calendar = Calendar.getInstance();
        	calendar.setTime(new Date(datePairs.get(position)[0]));
 
            return new SimpleDateFormat("MMM").format(calendar.getTime()) + " " + calendar.get(Calendar.YEAR);
        }
    }
    
}
