package com.calllogsvisualizer.ui;

import com.calllogsvisualizer.R;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceFragment;

public class SettingsSectionFragment extends PreferenceFragment {
	
	private static final int CYCLE_START_DATE = 1001;
//	private static final int CYCLE_LENGTH = 1002;
	
	SettingsSectionFragment self;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		this.self = this;
		
		// Load the preferences from an XML resource
		addPreferencesFromResource(R.xml.fragment_section_settings);

//		Preference myPref = (Preference) findPreference("pref_cycle_start_date");
//		myPref.setOnPreferenceClickListener(new OnPreferenceClickListener() {
//			public boolean onPreferenceClick(Preference preference) {
//				NumberPickerDialogFragment newFragment = new NumberPickerDialogFragment();
//				
//				SharedPreferences settings = getActivity().getSharedPreferences("pref_cycle_start_date", 0);
//				int cycleStartDate = settings.getInt("cycleStartDate", 1);
//				
//				Bundle args = new Bundle();
//			    args.putInt("value", cycleStartDate);
//			    newFragment.setArguments(args);
//			    
//				newFragment.show(getFragmentManager(), "dialog");
//				newFragment.setTargetFragment(self, CYCLE_START_DATE);
//				return true;
//			}
//		});

//		myPref = (Preference) findPreference("pref_cycle_length");
//		myPref.setOnPreferenceClickListener(new OnPreferenceClickListener() {
//			public boolean onPreferenceClick(Preference preference) {
//				NumberPickerDialogFragment newFragment = new NumberPickerDialogFragment();
//				
//				SharedPreferences settings = getActivity().getSharedPreferences("pref_cycle_length", 0);
//				int cycleLength = settings.getInt("cycleLength", 30);
//				
//				Bundle args = new Bundle();
//			    args.putInt("value", cycleLength);
//			    newFragment.setArguments(args);
//				
//				newFragment.show(getFragmentManager(), "dialog");
//				newFragment.setTargetFragment(self, CYCLE_LENGTH);
//				return true;
//			}
//		});
	}
	
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	    if (requestCode == CYCLE_START_DATE){
	        int selectedValue = data.getExtras().getInt("selectedNumber");
	        SharedPreferences settings = getActivity().getSharedPreferences("pref_cycle_start_date", 0);
	        SharedPreferences.Editor editor = settings.edit();
	        editor.putInt("cycleStartDate", selectedValue);
	        editor.commit();

	    }
//	    else if (requestCode == CYCLE_LENGTH) {
//	    	int selectedValue = data.getExtras().getInt("selectedNumber");
//	    	SharedPreferences settings = getActivity().getSharedPreferences("pref_cycle_length", 0);
//	        SharedPreferences.Editor editor = settings.edit();
//	        editor.putInt("cycleLength", selectedValue);
//	        editor.commit();
//	    }
	}

}
