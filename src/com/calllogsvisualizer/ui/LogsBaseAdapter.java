package com.calllogsvisualizer.ui;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import com.calllogsvisualizer.R;
import com.calllogsvisualizer.R.id;
import com.calllogsvisualizer.R.layout;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class LogsBaseAdapter extends BaseAdapter {
	
	ArrayList<LogsListData> logsList = new ArrayList<LogsListData>();
	LayoutInflater inflater;
    Context context;

	public LogsBaseAdapter(ArrayList<LogsListData> logsList, Context context) {
		super();
		this.logsList = logsList;
		this.context = context;
		inflater = LayoutInflater.from(this.context);
	}

	@Override
	public int getCount() {
		return logsList.size();
	}

	@Override
	public Object getItem(int position) {
		return logsList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return logsList.get(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder mViewHolder;
        
        if(convertView == null) {
                convertView = inflater.inflate(R.layout.logs_list_item, null);
                mViewHolder = new ViewHolder();
                convertView.setTag(mViewHolder);
        } else {
                mViewHolder = (ViewHolder) convertView.getTag();
        }
        
		mViewHolder.contactName = detail(convertView, R.id.contactName,
				logsList.get(position).getContactName());

		String paren = "";
		if (logsList.get(position).isInternational())
			paren = "International";
		else if (!logsList.get(position).isMobileNumber())
			paren = "Other";
		else
			paren = logsList.get(position).getOperator().getName();
		mViewHolder.numberAndOperator = detail(convertView,
				R.id.numberAndOperator, logsList.get(position).getNumber()
						+ " (" + paren + ")");

		mViewHolder.dateTime = detail(convertView, R.id.dateTime,
				logsList.get(position).getFormattedDatetime());

		mViewHolder.duration = detail(convertView, R.id.duration, "Duration: "
				+ logsList.get(position).getFormattedDuration());
        
        mViewHolder.iconPic  = detail(convertView, R.id.iconPic, logsList.get(position).getContactImgUri());
        
        return convertView;
	}
	
	private TextView detail(View v, int resId, String text) {
        TextView tv = (TextView) v.findViewById(resId);
        tv.setText(text);
        return tv;
	}
	
	private ImageView detail(View v, int resId, Uri uri) {
		ImageView iv = (ImageView) v.findViewById(resId);
		if(uri != null)
			iv.setImageURI(uri);
		else
			iv.setImageResource(R.drawable.ic_contact_picture);
		
		// If file not found or photo cannot be loaded 
		if (iv.getDrawable() == null) {
			iv.setImageResource(R.drawable.ic_contact_picture);
		}

		return iv;
	}

	private class ViewHolder {
		TextView contactName, numberAndOperator, dateTime, duration;
		ImageView iconPic;
	}

}
