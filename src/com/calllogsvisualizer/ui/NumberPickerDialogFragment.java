package com.calllogsvisualizer.ui;

import com.calllogsvisualizer.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.NumberPicker;

public class NumberPickerDialogFragment extends DialogFragment {
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	    // Get the layout inflater
	    LayoutInflater inflater = getActivity().getLayoutInflater();

	    View vNumberPicker = inflater.inflate(R.layout.number_picker, null);
	    
	    final NumberPicker numberPicker = (NumberPicker) vNumberPicker.findViewById(R.id.numberPicker);
	    numberPicker.setMaxValue(31);
	    numberPicker.setMinValue(1);
	    numberPicker.setValue(getArguments().getInt("value"));
	    
	    // Inflate and set the layout for the dialog
	    // Pass null as the parent view because its going in the dialog layout
	    builder.setView(vNumberPicker)
	    // Add action buttons
	    	   .setTitle("Usage Cycle")
	           .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
	               @Override
	               public void onClick(DialogInterface dialog, int id) {
	            	   Intent i = new Intent();
	            	   i.putExtra("selectedNumber", numberPicker.getValue());
	            	   getTargetFragment().onActivityResult(getTargetRequestCode(), 0, i);
	               }
	           })
	           .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
	               public void onClick(DialogInterface dialog, int id) {
//	            	   NumberPickerDialogFragment.this.getDialog().cancel();

	               }
	           });      
	    return builder.create();
	}

}
