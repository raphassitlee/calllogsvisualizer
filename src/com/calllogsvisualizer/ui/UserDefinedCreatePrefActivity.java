package com.calllogsvisualizer.ui;


import java.util.ArrayList;
import java.util.List;

import com.calllogsvisualizer.R;
import com.calllogsvisualizer.orm.Carrier;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class UserDefinedCreatePrefActivity extends Activity {
	
	EditText phoneNumber;
	Spinner spinner;
	
	public static final int RESULT_DELETE = 2003;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_defined_create_pref);

		// Set up action bar.
		final ActionBar actionBar = getActionBar();

		// Specify that the Home button should show an "Up" caret, indicating
		// that touching the
		// button will take the user one step up in the application's hierarchy.
		actionBar.setDisplayHomeAsUpEnabled(true);
		
		List<Carrier> carriers = Carrier.findAll();
		
		ArrayList<String> carrierNames = new ArrayList<String>();
		for (int i = 1; i < carriers.size(); i++) {
			carrierNames.add(carriers.get(i).getName());
		}
		
		phoneNumber = (EditText) findViewById(R.id.phoneNumber);
		
		spinner = (Spinner) findViewById(R.id.carrierSpinner);
		ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, carrierNames);
		spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(spinnerArrayAdapter);
		
		Button deleteBtn = (Button) findViewById(R.id.delete_button);
		deleteBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				setResult(RESULT_DELETE);
				finish();
			}
		});
		
		if(getIntent().getExtras() != null){
			if(getIntent().getExtras().containsKey("phoneNumber")) {
				phoneNumber.setText(getIntent().getExtras().getString("phoneNumber"));
			}
			if(getIntent().getExtras().containsKey("carrierName")) {
				int spinnerPosition = spinnerArrayAdapter.getPosition(getIntent().getExtras().getString("carrierName"));
				spinner.setSelection(spinnerPosition);
			}
		} else {
			deleteBtn.setVisibility(View.GONE);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu items for use in the action bar
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.user_defined_create_pref_actions, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// Respond to the action bar's Up/Home button
		case android.R.id.home:
			Intent upIntent = NavUtils.getParentActivityIntent(this);
			if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
				// This activity is NOT part of this app's task, so create a new
				// task
				// when navigating up, with a synthesized back stack.
				TaskStackBuilder.create(this)
				// Add all of this activity's parents to the back stack
						.addNextIntentWithParentStack(upIntent)
						// Navigate up to the closest parent
						.startActivities();
			} else {
				// This activity is part of this app's task, so simply
				// navigate up to the logical parent activity.
				NavUtils.navigateUpTo(this, upIntent);
			}
			return true;
		case R.id.action_done:
			// Prepare data intent for sending it back
			Intent data = new Intent();

			// Pass relevant data back as a result
			data.putExtra("phoneNumber", phoneNumber.getText().toString());
			data.putExtra("carrierName", spinner.getSelectedItem().toString());

			// Activity finished ok, return the data
			setResult(RESULT_OK, data); // set result code and bundle data for
										// response
			finish(); // closes the activity, pass data to parent
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
