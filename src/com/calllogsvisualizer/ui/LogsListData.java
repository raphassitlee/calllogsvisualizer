package com.calllogsvisualizer.ui;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import com.calllogsvisualizer.orm.Carrier;

import android.net.Uri;

public class LogsListData {
	
	private long id;
	
	private Uri contactImgUri;
	
	private String contactName;
	private String number;
	private boolean international;
	private boolean isMobileNumber;
	private Carrier operator;
	private Date datetime;
	private String duration;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getContactName() {
		return contactName;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public Carrier getOperator() {
		return operator;
	}
	public void setOperator(Carrier operator) {
		this.operator = operator;
	}
	public String getFormattedDatetime() {
		SimpleDateFormat sdf = new SimpleDateFormat("EEEE, dd MMMM yyyy HH:mm:ss");
		return sdf.format(getDatetime());
	}
	public Date getDatetime() {
		return datetime;
	}
	public void setDatetime(Date datetime) {
		this.datetime = datetime;
	}
	public String getDuration() {
		return duration;
	}
	public String getFormattedDuration() {
		Long seconds = Long.parseLong(getDuration());
		long hours = TimeUnit.SECONDS.toHours(seconds);
		long minute = TimeUnit.SECONDS.toMinutes(seconds)
				- (TimeUnit.SECONDS.toHours(seconds) * 60);
		long second = TimeUnit.SECONDS.toSeconds(seconds)
				- (TimeUnit.SECONDS.toMinutes(seconds) * 60);
		
		String strHours, strMinute, strSecond;
		if(hours < 10)
			strHours = "0" + hours;
		else
			strHours = "" + hours;
		if(minute < 10)
			strMinute = "0" + minute;
		else
			strMinute = "" + minute;
		if(second < 10)
			strSecond = "0" + second;
		else
			strSecond = "" + second;
		
		String fomattedDuration = strHours + ":" + strMinute + ":" + strSecond;
		
		return fomattedDuration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public Uri getContactImgUri() {
		return contactImgUri;
	}
	public void setContactImgUri(Uri contactImgUri) {
		this.contactImgUri = contactImgUri;
	}
	public boolean isInternational() {
		return international;
	}
	public void setInternational(boolean international) {
		this.international = international;
	}
	public boolean isMobileNumber() {
		return isMobileNumber;
	}
	public void setMobileNumber(boolean isMobileNumber) {
		this.isMobileNumber = isMobileNumber;
	}

}
