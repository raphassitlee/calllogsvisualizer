package com.calllogsvisualizer.ui;

import java.util.ArrayList;

import com.calllogsvisualizer.R;
import com.calllogsvisualizer.orm.Carrier;
import com.calllogsvisualizer.orm.UserDefinedNumber;
import com.calllogsvisualizer.ui.LogsSectionFragment.LogsListInitTask;
import com.calllogsvisualizer.util.CallLogsUtil;
import com.calllogsvisualizer.util.UsageUtil;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class UserDefinedPrefActivity extends Activity {
	
	private static final int NEW_USER_DEFINED = 2001;
	private static final int EDIT_USER_DEFINED = 2002;
	
	ListView lvUserDefined;
	UserDefinedBaseAdapter userDefinedAdapter;
	
	UserDefinedNumber selected;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_defined_pref);

		// Set up action bar.
		final ActionBar actionBar = getActionBar();

		// Specify that the Home button should show an "Up" caret, indicating
		// that touching the
		// button will take the user one step up in the application's hierarchy.
		actionBar.setDisplayHomeAsUpEnabled(true);
		
		lvUserDefined = (ListView) findViewById(R.id.userDefinedListView);
		lvUserDefined.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				selected = (UserDefinedNumber) userDefinedAdapter.getItem(position);
				Intent intent = new Intent(UserDefinedPrefActivity.this,
						UserDefinedCreatePrefActivity.class);
				if (intent != null) {
					intent.putExtra("phoneNumber", selected.getNumber());
					intent.putExtra("carrierName", Carrier.findById(selected.getCarrierId()).getName());
					// brings up the second activity
					startActivityForResult(intent, EDIT_USER_DEFINED);
				}
			}
		});
		
		initList();
	}
	
	private void initList() {
		ArrayList<UserDefinedNumber> userDefinedList = new ArrayList<UserDefinedNumber>(UserDefinedNumber.getAll());
		userDefinedAdapter = new UserDefinedBaseAdapter(userDefinedList, getApplicationContext());
		lvUserDefined.setAdapter(userDefinedAdapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu items for use in the action bar
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.user_defined_pref_actions, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// Respond to the action bar's Up/Home button
		case android.R.id.home:
			Intent upIntent = NavUtils.getParentActivityIntent(this);
			if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
				// This activity is NOT part of this app's task, so create a new
				// task
				// when navigating up, with a synthesized back stack.
				TaskStackBuilder.create(this)
				// Add all of this activity's parents to the back stack
						.addNextIntentWithParentStack(upIntent)
						// Navigate up to the closest parent
						.startActivities();
			} else {
				// This activity is part of this app's task, so simply
				// navigate up to the logical parent activity.
				NavUtils.navigateUpTo(this, upIntent);
			}
			return true;
		case R.id.action_new:
			Intent intent = new Intent(UserDefinedPrefActivity.this,
					UserDefinedCreatePrefActivity.class);
			if (intent != null) {
				// brings up the second activity
				startActivityForResult(intent, NEW_USER_DEFINED);
			}
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == NEW_USER_DEFINED) {
			if (resultCode == RESULT_OK) {
				String phoneNumber = data.getExtras().getString("phoneNumber");
				String carrierName = data.getExtras().getString("carrierName");
				
				UserDefinedNumber newUserDefined = new UserDefinedNumber(phoneNumber, Carrier.findByName(carrierName).getId());
				newUserDefined.save();

				initList();
				
				// re-retrieve call log
				CallLogsUtil.reInit();
				UsageUtil.reCreateDailyCarrierSummary();
				LogsSectionFragment.reInitList();
			}
		} else if (requestCode == EDIT_USER_DEFINED) {
			if (resultCode == RESULT_OK) {
				String phoneNumber = data.getExtras().getString("phoneNumber");
				String carrierName = data.getExtras().getString("carrierName");
				
				UserDefinedNumber newUserDefined = UserDefinedNumber.getByNumber(selected.getNumber());
				newUserDefined.setNumber(phoneNumber);
				newUserDefined.setCarrierId(Carrier.findByName(carrierName).getId());
				newUserDefined.save();
				
				initList();
				
				// re-retrieve call log
				CallLogsUtil.reInit();
				UsageUtil.reCreateDailyCarrierSummary();
				LogsSectionFragment.reInitList();
			} else if (resultCode == UserDefinedCreatePrefActivity.RESULT_DELETE) {
				UserDefinedNumber newUserDefined = UserDefinedNumber.getByNumber(selected.getNumber());
				newUserDefined.delete();
				
				initList();
				
				// re-retrieve call log
				CallLogsUtil.reInit();
				UsageUtil.reCreateDailyCarrierSummary();
				LogsSectionFragment.reInitList();
			}
		}
	}

}
