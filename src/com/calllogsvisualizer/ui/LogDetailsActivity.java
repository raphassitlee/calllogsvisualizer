package com.calllogsvisualizer.ui;

import java.util.Date;

import com.calllogsvisualizer.R;
import com.calllogsvisualizer.orm.CallerLocation;
import com.calllogsvisualizer.orm.Carrier;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class LogDetailsActivity extends Activity {
	
	private LogsListData logData;
	
	// Google Map
    private GoogleMap googleMap;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_log_details);

		// Set up action bar.
		final ActionBar actionBar = getActionBar();

		// Specify that the Home button should show an "Up" caret, indicating
		// that touching the
		// button will take the user one step up in the application's hierarchy.
		actionBar.setDisplayHomeAsUpEnabled(true);
		
		initData();
		initView();
		
		// Loading map
        initilizeMap();
	}
	
	private void initData() {
		Bundle extras = getIntent().getExtras();
		
		logData = new LogsListData();
		logData.setId(extras.getLong("id"));
		logData.setContactImgUri(extras.getString("contactImgUri").equals("") ? null : Uri.parse(extras.getString("contactImgUri")));
		logData.setContactName(extras.getString("contactName"));
		logData.setNumber(extras.getString("number"));
		logData.setInternational(extras.getBoolean("international"));
		logData.setMobileNumber(extras.getBoolean("mobileNumber"));
		if(!logData.isInternational() && logData.isMobileNumber())
			logData.setOperator(Carrier.findById(extras.getLong("operatorId")));
		logData.setDatetime(new Date(extras.getLong("datetime")));
		logData.setDuration(extras.getString("duration"));
	}
	
	private void initView() {
		setTextViewText(R.id.contactName, logData.getContactName());
		setTextViewText(R.id.number, logData.getNumber());
		if(logData.getOperator() != null)
			setTextViewText(R.id.operator, logData.getOperator().getName());
		else if(logData.isInternational())
			setTextViewText(R.id.operator, "International");
		else if(!logData.isMobileNumber())
			setTextViewText(R.id.operator, "Other");
		else
			setTextViewText(R.id.operator, "Unknown");
		setTextViewText(R.id.dateTime, logData.getFormattedDatetime());
		setTextViewText(R.id.duration, "Duration: " + logData.getFormattedDuration());
		setImageViewUri(R.id.iconPic, logData.getContactImgUri());
	}
	
	private void setTextViewText(int resId, String text) {
        TextView tv = (TextView) findViewById(resId);
        tv.setText(text);
	}
	
	private void setImageViewUri(int resId, Uri uri) {
		ImageView iv = (ImageView) findViewById(resId);
		if(uri != null)
			iv.setImageURI(uri);
		else
			iv.setImageResource(R.drawable.ic_contact_picture);
		
		// If file not found or photo cannot be loaded 
		if (iv.getDrawable() == null) {
			iv.setImageResource(R.drawable.ic_contact_picture);
		}
	}
	
	/**
     * function to load map. If map is not created it will create it for you
     * */
    private void initilizeMap() {
    	CallerLocation callLocation = CallerLocation.findLocationByCallLogId(logData.getId());
    	
    	if (callLocation != null) {
    		findViewById(R.id.mapFrame).setVisibility(View.VISIBLE);
    		findViewById(R.id.noLocation).setVisibility(View.GONE);
	        if (googleMap == null) {
	            googleMap = ((MapFragment) getFragmentManager().findFragmentById(
	                    R.id.map)).getMap();
	 
	            // check if map is created successfully or not
	            if (googleMap == null) {
	                Toast.makeText(getApplicationContext(),
	                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
	                        .show();
	            }
	        }

	        // latitude and longitude
	        double latitude = callLocation.getLatitude();
	        double longitude = callLocation.getLongitude();
	        
	        CameraPosition cameraPosition = new CameraPosition.Builder().target(
	                new LatLng(latitude, longitude)).zoom(12).build();
	 
	        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
	         
	        // create marker
	        MarkerOptions marker = new MarkerOptions().position(new LatLng(latitude, longitude)).title("Call Location");
	         
	        // adding marker
	        googleMap.addMarker(marker);
    	}
    }

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// Respond to the action bar's Up/Home button
		case android.R.id.home:
			Intent upIntent = NavUtils.getParentActivityIntent(this);
			if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
				// This activity is NOT part of this app's task, so create a new
				// task
				// when navigating up, with a synthesized back stack.
				TaskStackBuilder.create(this)
				// Add all of this activity's parents to the back stack
						.addNextIntentWithParentStack(upIntent)
						// Navigate up to the closest parent
						.startActivities();
			} else {
				// This activity is part of this app's task, so simply
				// navigate up to the logical parent activity.
				NavUtils.navigateUpTo(this, upIntent);
			}
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
    protected void onResume() {
        super.onResume();
        initilizeMap();
    }

}
