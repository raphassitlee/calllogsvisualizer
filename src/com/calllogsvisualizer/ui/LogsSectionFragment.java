package com.calllogsvisualizer.ui;

import java.util.ArrayList;

import com.calllogsvisualizer.R;
import com.calllogsvisualizer.util.CallLogsUtil;
import com.calllogsvisualizer.util.UsageUtil;

import android.os.AsyncTask;
import android.os.Bundle;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Fragment;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

/**
 * A dummy fragment representing a section of the app, but that simply displays
 * dummy text.
 */
public class LogsSectionFragment extends Fragment {

	static View rootView;
	
	View mLoadingView;
	static ListView lvLogs;
	static LogsBaseAdapter logsAdapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_section_logs,
				container, false);

		mLoadingView = rootView.findViewById(R.id.loading_spinner);
		
		lvLogs = (ListView) rootView.findViewById(R.id.lvLogsList);
		lvLogs.setVisibility(View.GONE);
		lvLogs.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				LogsListData logData = (LogsListData) logsAdapter.getItem(position);
				Intent logDetailsIntent = new Intent(rootView.getContext(), LogDetailsActivity.class);
				logDetailsIntent.putExtra("id", logData.getId());
				logDetailsIntent.putExtra("contactImgUri", logData.getContactImgUri() == null ? "" : logData.getContactImgUri().toString());
				logDetailsIntent.putExtra("contactName", logData.getContactName());
				logDetailsIntent.putExtra("number", logData.getNumber());
				logDetailsIntent.putExtra("international", logData.isInternational());
				if(!logData.isInternational() && logData.isMobileNumber())
					logDetailsIntent.putExtra("operatorId", logData.getOperator().getId());
				logDetailsIntent.putExtra("mobileNumber", logData.isMobileNumber());
				logDetailsIntent.putExtra("datetime", logData.getDatetime().getTime());
				logDetailsIntent.putExtra("duration", logData.getDuration());
				startActivity(logDetailsIntent);
			}
		});
		
		new LogsListInitTask().execute();

		return rootView;
	}
	
	private void showContent() {
		int mShortAnimationDuration = getResources().getInteger(android.R.integer.config_shortAnimTime);
		
        // Decide which view to hide and which to show.
        final View showView = lvLogs;
        final View hideView = mLoadingView;

        // Set the "show" view to 0% opacity but visible, so that it is visible
        // (but fully transparent) during the animation.
        showView.setAlpha(0f);
        showView.setVisibility(View.VISIBLE);

        // Animate the "show" view to 100% opacity, and clear any animation listener set on
        // the view. Remember that listeners are not limited to the specific animation
        // describes in the chained method calls. Listeners are set on the
        // ViewPropertyAnimator object for the view, which persists across several
        // animations.
        showView.animate()
                .alpha(1f)
                .setDuration(mShortAnimationDuration)
                .setListener(null);

        // Animate the "hide" view to 0% opacity. After the animation ends, set its visibility
        // to GONE as an optimization step (it won't participate in layout passes, etc.)
        hideView.animate()
                .alpha(0f)
                .setDuration(mShortAnimationDuration)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        hideView.setVisibility(View.GONE);
                    }
                });
    }
	
	public static void reInitList() {
		logsAdapter = new LogsBaseAdapter(CallLogsUtil.getCallDetails(rootView.getContext()), rootView.getContext());
		lvLogs.setAdapter(logsAdapter);
	}

	public class LogsListInitTask extends AsyncTask<Void, Void, ArrayList<LogsListData>> {

		@Override
		protected ArrayList<LogsListData> doInBackground(Void... params) {
			ArrayList<LogsListData> logs = CallLogsUtil.getCallDetails(getActivity());
			return logs;
		}
		
		protected void onPostExecute(ArrayList<LogsListData> result){
			logsAdapter = new LogsBaseAdapter(result, rootView.getContext());
			lvLogs.setAdapter(logsAdapter);
//			mLoadingView.setVisibility(View.GONE);
//			lvLogs.setVisibility(View.VISIBLE);
			showContent();
		}
		
	}
}
