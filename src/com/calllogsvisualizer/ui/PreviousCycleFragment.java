package com.calllogsvisualizer.ui;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.androidplot.pie.PieChart;
import com.androidplot.pie.Segment;
import com.androidplot.pie.SegmentFormatter;
import com.calllogsvisualizer.R;
import com.calllogsvisualizer.orm.Carrier;
import com.calllogsvisualizer.orm.DailyCarrierSummary;
import com.calllogsvisualizer.orm.DailyRegionSummary;
import com.calllogsvisualizer.util.CallLogsUtil;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class PreviousCycleFragment extends Fragment {

	public static final String ARG_START_DATE = "startDate";
	public static final String ARG_END_DATE = "endDate";
	
	private static final int RED = Color.parseColor("#f03131");
	private static final int ORANGE = Color.parseColor("#ffae18");
	private static final int GREEN = Color.parseColor("#83b600");
	private static final int AQUA = Color.parseColor("#16a5d7");
	private static final int GREY = Color.parseColor("#999999");
	
	private static final int PURPLE = Color.parseColor("#c841ea");
	private static final int YELLOW = Color.parseColor("#eae825");

	private View rootView;
	
	private View usageBarScrollView, usagePieScrollView;
	
	long startDate, endDate;

//	private ArrayList<RelativeLayout> operatorBarItems = new ArrayList<RelativeLayout>();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater.inflate(
				R.layout.fragment_previous_cycle, container, false);

		Bundle args = getArguments();
		startDate = args.getLong(ARG_START_DATE);
		endDate = args.getLong(ARG_END_DATE);
		
		usageBarScrollView = rootView.findViewById(R.id.usageBarScrollView);
		View barFragment = rootView.findViewById(R.id.usageBarView);
		barFragment.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				showContent(true);
			}
		});
		usagePieScrollView = rootView.findViewById(R.id.usagePieScrollView);
		View pieFragment = rootView.findViewById(R.id.usagePieView);
		pieFragment.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				showContent(false);
			}
		});
		
		initBarCharts();
		
		return rootView;
	}
	
	private void initBarCharts() {

//		if(!operatorBarItems.isEmpty())
//			((ViewGroup) operatorBarItems.get(0).getParent()).removeAllViews();
//		
//		operatorBarItems = new ArrayList<RelativeLayout>();
		
		SegmentFormatter sf1 = new SegmentFormatter();
		sf1.configure(rootView.getContext(), R.xml.pie_segment_formatter_red);

		sf1.getFillPaint();

		SegmentFormatter sf2 = new SegmentFormatter();
		sf2.configure(rootView.getContext(), R.xml.pie_segment_formatter_orange);

		sf2.getFillPaint();

		SegmentFormatter sf3 = new SegmentFormatter();
		sf3.configure(rootView.getContext(), R.xml.pie_segment_formatter_green);

		sf3.getFillPaint();

		SegmentFormatter sf4 = new SegmentFormatter();
		sf4.configure(rootView.getContext(), R.xml.pie_segment_formatter_aqua);

		sf4.getFillPaint();
		
		SegmentFormatter sf5 = new SegmentFormatter();
		sf5.configure(rootView.getContext(), R.xml.pie_segment_formatter_grey);

		sf5.getFillPaint();
		
		SegmentFormatter sf6 = new SegmentFormatter();
		sf6.configure(rootView.getContext(), R.xml.pie_segment_formatter_purple);

		sf6.getFillPaint();
		
		SegmentFormatter sf7 = new SegmentFormatter();
		sf7.configure(rootView.getContext(), R.xml.pie_segment_formatter_yellow);

		sf7.getFillPaint();
		
		long durationDomestic = 0;
		
		// Regions
		List<DailyRegionSummary> dataRegion = DailyRegionSummary.getByDateRange(startDate, endDate);
		
		if(dataRegion.size() == 0) {
			rootView.findViewById(R.id.usageBarView).findViewById(R.id.noUsageByRegionTextView).setVisibility(View.VISIBLE);
			rootView.findViewById(R.id.usagePieView).findViewById(R.id.noUsageByRegionTextView).setVisibility(View.VISIBLE);
		} else {
		
			long totalDuration = 0;
			
			DailyRegionSummary dataDomestic = null, dataInternational = null;
			
			for (int i = 0; i < dataRegion.size(); i++) {
				if(dataRegion.get(i).isInternational())
					dataInternational = dataRegion.get(i);
				else {
					dataDomestic = dataRegion.get(i);
					durationDomestic = dataRegion.get(i).getDuration();
				}
				totalDuration += dataRegion.get(i).getDuration();
			}
			
			RelativeLayout item;
			TextView tvOperatorName, tvDuration;
			ProgressBar progressBar;
			
			PieChart regionsPie = (PieChart) rootView.findViewById(R.id.regionsPieChart);
			
			if(dataDomestic != null){
				item = (RelativeLayout) rootView.findViewById(R.id.domestic);
				item.setVisibility(View.VISIBLE);
		
				tvOperatorName = (TextView) item.findViewById(R.id.domesticText);
				tvOperatorName.setText("Domestic");
				tvDuration = (TextView) item.findViewById(R.id.durationDomestic);
				tvDuration.setText(CallLogsUtil.getFormattedDuration(dataDomestic.getDuration())+" min");
				
				progressBar = (ProgressBar) item.findViewById(R.id.progressBarDomestic);
				if(totalDuration != 0) {
					progressBar.setProgress(Math.round((float)dataDomestic.getDuration() / totalDuration * 100));
				}
				setBarColor(progressBar, PURPLE);
				
				Segment domestic = new Segment("Domestic", dataDomestic.getDuration());
				regionsPie.addSeries(domestic, sf6);
			}
			if(dataInternational != null) {
				item = (RelativeLayout) rootView.findViewById(R.id.international);
				item.setVisibility(View.VISIBLE);
		
				tvOperatorName = (TextView) item.findViewById(R.id.internationalText);
				tvOperatorName.setText("International");
				tvDuration = (TextView) item.findViewById(R.id.durationInternational);
				tvDuration.setText(CallLogsUtil.getFormattedDuration(dataInternational.getDuration())+" min");
				
				progressBar = (ProgressBar) item.findViewById(R.id.progressBarInternational);
				if(totalDuration != 0) {
					progressBar.setProgress(Math.round((float)dataInternational.getDuration() / totalDuration * 100));
				}
				setBarColor(progressBar, YELLOW);
				
				Segment international = new Segment("International",
						dataInternational.getDuration());
				regionsPie.addSeries(international, sf7);
			}
			
			regionsPie.getBorderPaint().setColor(Color.TRANSPARENT);
			regionsPie.getBackgroundPaint().setColor(Color.TRANSPARENT);
		}
		
		List<DailyCarrierSummary> data = DailyCarrierSummary.getByDateRange(startDate, endDate);
		
		PieChart operatorsPie = (PieChart) rootView.findViewById(R.id.operatorsPieChart);
		
		if(data.size() == 0) {
			if(durationDomestic > 0) {
				RelativeLayout item = (RelativeLayout) rootView.findViewById(R.id.operator5);
				item.setVisibility(View.VISIBLE);
	
				TextView tvOperatorName = (TextView) item.findViewById(R.id.operatorName5);
				tvOperatorName.setText("Other");
				TextView tvDuration = (TextView) item.findViewById(R.id.duration5);
				tvDuration.setText(CallLogsUtil.getFormattedDuration(durationDomestic)+" min");
				
				ProgressBar progressBar = (ProgressBar) item.findViewById(R.id.progressBar5);
				progressBar.setProgress(100);
				setBarColor(progressBar, GREY);
				
				Segment s5 = new Segment("Other", durationDomestic);
				operatorsPie.addSeries(s5, sf5);
			} else {
				rootView.findViewById(R.id.usageBarView).findViewById(R.id.noUsageByOperatorTextView).setVisibility(View.VISIBLE);
				rootView.findViewById(R.id.usagePieView).findViewById(R.id.noUsageByOperatorTextView).setVisibility(View.VISIBLE);
			}
		} else {
			
			DailyCarrierSummary dataOptus = null, dataVodafone = null, dataTeltra = null, dataLycamobile = null;
		
			long totalDuration = 0;
		
			for (int i = 0; i < data.size(); i++) {
				if(data.get(i).getCarrierId() == 1)
					dataTeltra = data.get(i);
				else if(data.get(i).getCarrierId() == 2)
					dataOptus = data.get(i);
				else if(data.get(i).getCarrierId() == 3)
					dataVodafone = data.get(i);
				else if(data.get(i).getCarrierId() == 4)
					dataLycamobile = data.get(i);
				totalDuration += data.get(i).getDuration();
			}
			
			RelativeLayout item;
			TextView tvOperatorName, tvDuration;
			ProgressBar progressBar;
			
			if(dataOptus != null) {
				item = (RelativeLayout) rootView.findViewById(R.id.operator1);
				item.setVisibility(View.VISIBLE);
	
				tvOperatorName = (TextView) item.findViewById(R.id.operatorName1);
				tvOperatorName.setText(Carrier.findById(dataOptus.getCarrierId()).getName());
				tvDuration = (TextView) item.findViewById(R.id.duration1);
				tvDuration.setText(CallLogsUtil.getFormattedDuration(dataOptus.getDuration())+" min");
				
				progressBar = (ProgressBar) item.findViewById(R.id.progressBar1);
				if(totalDuration != 0) {
					progressBar.setProgress(Math.round((float)dataOptus.getDuration() / totalDuration * 100));
				}
				setBarColor(progressBar, ORANGE);
				
				Segment s2 = new Segment("Optus", dataOptus.getDuration());
				operatorsPie.addSeries(s2, sf2);
			}
			if(dataVodafone != null) {
				item = (RelativeLayout) rootView.findViewById(R.id.operator2);
				item.setVisibility(View.VISIBLE);
	
				tvOperatorName = (TextView) item.findViewById(R.id.operatorName2);
				tvOperatorName.setText(Carrier.findById(dataVodafone.getCarrierId()).getName());
				tvDuration = (TextView) item.findViewById(R.id.duration2);
				tvDuration.setText(CallLogsUtil.getFormattedDuration(dataVodafone.getDuration())+" min");
				
				progressBar = (ProgressBar) item.findViewById(R.id.progressBar2);
				if(totalDuration != 0) {
					progressBar.setProgress(Math.round((float)dataVodafone.getDuration() / totalDuration * 100));
				}
				setBarColor(progressBar, RED);
				
				Segment s1 = new Segment("Vodafone", dataVodafone.getDuration());
				operatorsPie.addSeries(s1, sf1);
			}
			if(dataTeltra != null) {
				item = (RelativeLayout) rootView.findViewById(R.id.operator3);
				item.setVisibility(View.VISIBLE);
	
				tvOperatorName = (TextView) item.findViewById(R.id.operatorName3);
				tvOperatorName.setText(Carrier.findById(dataTeltra.getCarrierId()).getName());
				tvDuration = (TextView) item.findViewById(R.id.duration3);
				tvDuration.setText(CallLogsUtil.getFormattedDuration(dataTeltra.getDuration())+" min");
				
				progressBar = (ProgressBar) item.findViewById(R.id.progressBar3);
				if(totalDuration != 0) {
					progressBar.setProgress(Math.round((float)dataTeltra.getDuration() / totalDuration * 100));
				}
				setBarColor(progressBar, AQUA);
				
				Segment s4 = new Segment("Telstra", dataTeltra.getDuration());
				operatorsPie.addSeries(s4, sf4);
			}
			if(dataLycamobile != null) {
				item = (RelativeLayout) rootView.findViewById(R.id.operator4);
				item.setVisibility(View.VISIBLE);
	
				tvOperatorName = (TextView) item.findViewById(R.id.operatorName4);
				tvOperatorName.setText(Carrier.findById(dataLycamobile.getCarrierId()).getName());
				tvDuration = (TextView) item.findViewById(R.id.duration4);
				tvDuration.setText(CallLogsUtil.getFormattedDuration(dataLycamobile.getDuration())+" min");
				
				progressBar = (ProgressBar) item.findViewById(R.id.progressBar4);
				if(totalDuration != 0) {
					progressBar.setProgress(Math.round((float)dataLycamobile.getDuration() / totalDuration * 100));
				}
				setBarColor(progressBar, GREEN);
				
				Segment s3 = new Segment("Lycamobile", dataLycamobile.getDuration());
				operatorsPie.addSeries(s3, sf3);
			}
			if(durationDomestic - totalDuration > 0) {
				item = (RelativeLayout) rootView.findViewById(R.id.operator5);
				item.setVisibility(View.VISIBLE);
	
				tvOperatorName = (TextView) item.findViewById(R.id.operatorName5);
				tvOperatorName.setText("Other");
				tvDuration = (TextView) item.findViewById(R.id.duration5);
				tvDuration.setText(CallLogsUtil.getFormattedDuration(durationDomestic - totalDuration)+" min");
				
				progressBar = (ProgressBar) item.findViewById(R.id.progressBar5);
				if(totalDuration != 0) {
					progressBar.setProgress(Math.round((float)(durationDomestic - totalDuration) / totalDuration * 100));
				}
				setBarColor(progressBar, GREY);
				
				Segment s5 = new Segment("Other", durationDomestic - totalDuration);
				operatorsPie.addSeries(s5, sf5);
			}
		}
		
		operatorsPie.getBorderPaint().setColor(Color.TRANSPARENT);
		operatorsPie.getBackgroundPaint().setColor(Color.TRANSPARENT);
	}
	
	private void setBarColor(ProgressBar progressBar, int color) {
		Resources res = getResources();
		Rect bounds = progressBar.getProgressDrawable().getBounds();

		int drawableId = R.drawable.usage_operator_bar_aqua;
		
		if(color == RED)
			drawableId = R.drawable.usage_operator_bar_red;
		else if (color == ORANGE)
			drawableId = R.drawable.usage_operator_bar_orange;
		else if (color == GREEN)
			drawableId = R.drawable.usage_operator_bar_green;
		else if (color == AQUA)
			drawableId = R.drawable.usage_operator_bar_aqua;
		else if (color == GREY)
			drawableId = R.drawable.usage_operator_bar_grey;
		else if (color == PURPLE)
			drawableId = R.drawable.usage_operator_bar_purple;
		else if (color == YELLOW)
			drawableId = R.drawable.usage_operator_bar_yellow;
			
		progressBar.setProgressDrawable(res
				.getDrawable(drawableId));

		progressBar.getProgressDrawable().setBounds(bounds);
	}
	
	private void showContent(boolean front) {
		int mShortAnimationDuration = getResources().getInteger(android.R.integer.config_shortAnimTime);
		
        // Decide which view to hide and which to show.
        final View showView = front ? usagePieScrollView : usageBarScrollView;
        final View hideView = front ? usageBarScrollView : usagePieScrollView;

        // Set the "show" view to 0% opacity but visible, so that it is visible
        // (but fully transparent) during the animation.
        showView.setAlpha(0f);
        showView.setVisibility(View.VISIBLE);

        // Animate the "show" view to 100% opacity, and clear any animation listener set on
        // the view. Remember that listeners are not limited to the specific animation
        // describes in the chained method calls. Listeners are set on the
        // ViewPropertyAnimator object for the view, which persists across several
        // animations.
        showView.animate()
                .alpha(1f)
                .setDuration(mShortAnimationDuration)
                .setListener(null);

        // Animate the "hide" view to 0% opacity. After the animation ends, set its visibility
        // to GONE as an optimization step (it won't participate in layout passes, etc.)
        hideView.animate()
                .alpha(0f)
                .setDuration(mShortAnimationDuration)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        hideView.setVisibility(View.GONE);
                    }
                });
    }

}
