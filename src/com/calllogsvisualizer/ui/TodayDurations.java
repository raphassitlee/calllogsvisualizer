package com.calllogsvisualizer.ui;

public class TodayDurations {
	
	private long todayDurationDomestic = 0;
	private long todayDurationInternational = 0;
	private long todayDurationOptus = 0;
	private long todayDurationVodafone = 0;
	private long todayDurationTelstra = 0;
	private long todayDurationLycamobile = 0;
	
	public long getTodayDurationDomestic() {
		return todayDurationDomestic;
	}
	public void setTodayDurationDomestic(long todayDurationDomestic) {
		this.todayDurationDomestic = todayDurationDomestic;
	}
	public long getTodayDurationInternational() {
		return todayDurationInternational;
	}
	public void setTodayDurationInternational(long todayDurationInternational) {
		this.todayDurationInternational = todayDurationInternational;
	}
	public long getTodayDurationOptus() {
		return todayDurationOptus;
	}
	public void setTodayDurationOptus(long todayDurationOptus) {
		this.todayDurationOptus = todayDurationOptus;
	}
	public long getTodayDurationVodafone() {
		return todayDurationVodafone;
	}
	public void setTodayDurationVodafone(long todayDurationVodafone) {
		this.todayDurationVodafone = todayDurationVodafone;
	}
	public long getTodayDurationTelstra() {
		return todayDurationTelstra;
	}
	public void setTodayDurationTelstra(long todayDurationTelstra) {
		this.todayDurationTelstra = todayDurationTelstra;
	}
	public long getTodayDurationLycamobile() {
		return todayDurationLycamobile;
	}
	public void setTodayDurationLycamobile(long todayDurationLycamobile) {
		this.todayDurationLycamobile = todayDurationLycamobile;
	}

}
